#include <iostream>
#include "algorithm/aerial_display.h"
#include "algorithm/parameters.h"
#include "algorithm/parse_exception.h"

int main(int argc, char ** argv) {
    try {
        aed::parameters input(std::cin);
        aed::aerial_display display(input.get_graph());
        auto paths = display.display_frames(input.get_frames());

        if (argc >= 2 && std::string(argv[1]) == "simplePrint") {
            for (const auto & path : paths) {
                if (path == nullptr) {
                    std::cout << "No path found" << std::endl;
                } else {
                    std::cout << *path << std::endl;
                }
            }
        } else {
            input.set_frame_paths(std::move(paths));
            input.save(std::cout, true);
        }
    } catch (aed::parse_exception & e) {
        std::cerr << "Parse error:" << std::endl;
        std::cerr << "Line: " << e.get_line() << std::endl;
        std::cerr << "Row: " << e.get_content() << std::endl;
        std::cerr << "Reason: " << e.get_reason() << std::endl;
        return 1;
    } catch (std::exception & e) {
        std::cerr << "Error: " << e.what() << std::endl;
        return 1;
    }

    return 0;
}
