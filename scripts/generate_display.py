#!/usr/bin/env python3

import argparse
import random


def parse_params():
    parser = argparse.ArgumentParser(description="Generates random map configuration for AED display.")
    graph_group = parser.add_argument_group("Display settings")
    graph_group.add_argument("--length", type=int, required=True, help="Number of pixels in length dimension.")
    graph_group.add_argument("--width", type=int, required=True, help="Number of pixels in width dimension.")
    graph_group.add_argument("--height", type=int, required=True, help="Number of pixels in height dimension.")
    graph_group.add_argument("-k", type=int, required=False,
                             help="Defines path connectivity between pixels. Minimum value is 2.", default=3)
    graph_group.add_argument("--precision", type=int, required=False,
                             help="Precision of path lengths (number of points "
                                  "for measuring lengths of curved "
                                  "trajectories).",
                             default=100)
    graph_group.add_argument("--curvature", type=float, required=False,
                             help="Curvature of paths. Higher number tends to "
                                  "generate longer paths.", default=1.0)
    graph_group.add_argument("--grid_size", type=float, required=True,
                             help="Distance between pixel points (in millimetres).")
    graph_group.add_argument("--distance_multiplier", type=float, required=False,
                             help="Sets the importance of remaining path lengths to the path finding algorithm. If the "
                                  "importance is set low, the algorithm may take a long time to compute paths.",
                             default=5)

    drone_group = parser.add_argument_group("Drone settings")
    drone_group.add_argument("--drone_count", type=int, required=True, help="Number of drones.")
    drone_group.add_argument("--drone_density", type=float, required=False,
                             help="Number between 0 and 1. Density of drones in "
                                  "each frame. 1 means drones are packed "
                                  "together, 0 means drones are scattered "
                                  "across the display.", default=0.0)
    drone_group.add_argument("--fixed_drones", type=int, required=False,
                             help="Number of drones having their position fixed.",
                             default=0)

    drone_group.add_argument("--max_speed", type=float, required=True,
                             help="Maximum speed of drones measured in m * s^(-1).")
    drone_group.add_argument("--max_acc", type=float, required=True,
                             help="Maximum acceleration of drones measured in m * "
                                  "s^(-2).")
    drone_group.add_argument("--drone_length", type=float, required=True, help="Drone length radius (in millimetres).")
    drone_group.add_argument("--drone_width", type=float, required=True, help="Drone width radius (in millimetres).")
    drone_group.add_argument("--drone_height", type=float, required=True, help="Drone height radius (in millimetres).")

    drone_group.add_argument("--stop_penalty", type=float, required=False, help="Time (in milliseconds) that penalizes "
                                                                                "stopping at a given location.",
                             default=0.0)

    frames_group = parser.add_argument_group("Frames settings")
    frames_group.add_argument("--frames_count", type=int, required=True, help="Number of frames.")
    frames_group.add_argument("--wait_time", type=float, required=True,
                              help="How long should stay drones in position after they reach their destination.")

    return parser.parse_args()


def print_graph_settings(args):
    print("[graph]")
    print(f"length={args.length}")
    print(f"width={args.width}")
    print(f"height={args.height}")
    print(f"k={args.k}")
    print(f"precision={args.precision}")
    print(f"curvature={args.curvature}")
    print(f"grid_size={args.grid_size}")
    print(f"drone_length={args.drone_length}")
    print(f"drone_width={args.drone_width}")
    print(f"drone_height={args.drone_height}")
    print(f"max_speed={args.max_speed}")
    print(f"max_acceleration={args.max_acc}")
    print(f"stop_penalty={args.stop_penalty}")
    print(f"distance_multiplier={args.distance_multiplier}")
    print(f"[/graph]")


def print_display_settings(args):
    print("[display]")
    print(f"drone_count={args.drone_count}")
    print(f"wait_time={args.wait_time}")
    print(f"[frames]")
    print_edge_frame(args, True)
    for i in range(args.frames_count):
        print_frame(args)
    print_edge_frame(args, False)
    print("[/frames]")
    print("[/display]")


def validate_args(args):
    if args.length <= 0 or args.width <= 0 or args.height <= 0:
        raise ValueError("Incorrect dimension sizes of the display.")

    if args.drone_count > args.width * args.length:
        raise ValueError(f"Exceeded the number of drones (the maximum is width * length). Got {args.drone_count}, the "
                         f"max is {args.width * args.length}")

    if args.fixed_drones < 0 or args.fixed_drones > args.drone_count:
        raise ValueError(f"Number of fixed drones is out of range [0, {args.drone_count}]. Got: {args.fixed_drones}.")

    if args.frames_count < 0:
        raise ValueError(f"Negative number of frames! Got {args.frames_count}")

    if args.drone_density < 0 or args.drone_density > 1:
        raise ValueError(f"Drone density is out of range [0, 1]. Got {args.drone_density}.")


def print_position(x, y, z, agent):
    print("[position]")
    print(f"agent={agent}")
    print("[coordinates]")
    print(f"x={x}")
    print(f"y={y}")
    print(f"z={z}")
    print("[/coordinates]")

    print("[color]")
    print(f"red={random.randint(0, 255)}")
    print(f"green={random.randint(0, 255)}")
    print(f"blue={random.randint(0, 255)}")
    print("active=1")
    print("[/color]")

    print("[/position]")


def print_edge_frame(args, first):
    max_index = args.width * args.length

    picked = random.sample(range(max_index), args.drone_count)

    if first:
        drone_assignment = [*range(args.drone_count)]
        random.shuffle(drone_assignment)
    else:
        drone_assignment = random.sample(range(args.drone_count), args.fixed_drones)

    j = 0

    print("[frame]")

    for i in picked:
        x = int(i // args.width)
        y = i % args.width

        drone = -1 if j >= len(drone_assignment) else drone_assignment[j]
        j += 1

        print_position(x, y, 0, drone)

    print("[/frame]")


def rand_coordinate(args, dim_size):
    deviation = ((dim_size - args.drone_count ** (1 / 3)) * (1 - args.drone_density) + args.drone_count ** (1 / 3)) / 2
    return int(round(random.gauss(dim_size / 2, deviation))) % dim_size


def print_frame(args):
    drone_assignment = random.sample(range(args.drone_count), args.fixed_drones)
    picked = set()

    print("[frame]")
    for i in range(args.drone_count):
        while len(picked) == i:
            x = rand_coordinate(args, args.length)
            y = rand_coordinate(args, args.width)
            z = rand_coordinate(args, args.height)

            coord = (x, y, z)
            picked.add(coord)

        drone = -1 if i >= len(drone_assignment) else drone_assignment[i]
        print_position(coord[0], coord[1], coord[2], drone)

    print("[/frame]")


args = parse_params()
print_graph_settings(args)
print()
print_display_settings(args)
