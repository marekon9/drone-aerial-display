#!/usr/bin/env python3
import argparse
import csv

parser = argparse.ArgumentParser()
parser.add_argument("-b", type=str, dest="base")
parser.add_argument("-c", type=str, dest="compare")
args = parser.parse_args()

acc_ratio = 0.0
succ_count = 0

with open(args.base, "r") as f_base, open(args.compare, "r") as f_compare:
    csv_base = csv.reader(f_base, delimiter=";", quoting=csv.QUOTE_NONE)
    csv_compare = csv.reader(f_compare, delimiter=";", quoting=csv.QUOTE_NONE)

    for (base, compare) in list(zip(csv_base, csv_compare))[1:]:
        b_success = base[2] == "True"
        c_success = compare[2] == "True"

        b_res = float(base[3])
        c_res = float(compare[3])

        if b_success and c_success:
            ratio = c_res / b_res
            acc_ratio += ratio
            succ_count += 1

            print(f"ratio for {base[0]} and {compare[0]} is {ratio}.")
        else:
            print(f"cannot compare {base[0]} and {compare[0]}")

print("=====")
print("")
print(f"Results ratio of {args.base} and {args.compare} is: {(acc_ratio / succ_count) if succ_count > 0 else 0}")
