#!/usr/bin/env python3

import argparse
import datetime
import os
import subprocess
import threading
from timeit import timeit, Timer

import resource
import sys

total_succ_time = 0.0
total_success = 0


def parse_params():
    parser = argparse.ArgumentParser()
    parser.add_argument("-b", type=str, required=True, dest='binary')
    parser.add_argument("-m", type=int, required=True, dest='memory')
    parser.add_argument("-t", type=float, required=True, dest='time')
    parser.add_argument("-i", nargs='+', type=str, dest='inputs')
    parser.add_argument("-o", required=True, dest='output_dir')
    return parser.parse_args()


def launch_process(args, in_file, out_file):
    try:
        with open(in_file, 'rb') as f_stdin:
            p_in = f_stdin.read()

        p = subprocess.Popen([args.binary], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        giga_byte = 2 ** 30
        resource.prlimit(p.pid, resource.RLIMIT_AS, (args.memory * giga_byte, args.memory * giga_byte))

        stdout, _ = p.communicate(p_in, args.time)

        with open(out_file, 'wb') as f_stdout:
            f_stdout.write(stdout)
    except subprocess.TimeoutExpired:
        p.kill()


def execute_process(args, input: str, res):
    now = datetime.datetime.now()
    print(f"{now.strftime('%Y-%m-%d %H:%M:%S')}: Compute {input}")
    sys.stdout.flush()

    name = input.split("/")[-1]
    out_name = f"{args.output_dir}/{name}"
    time = Timer(lambda: launch_process(args, input, out_name)).timeit(1)
    cost = -1.0

    if os.path.isfile(out_name):
        with open(out_name, 'r') as res_file:
            for line in res_file.readlines():
                prefix = "total_cost="
                if line.startswith(prefix):
                    cost = float(line.split("=")[-1].strip())
                    break

    global total_succ_time, total_success

    if cost >= 0:
        total_success += 1
        total_succ_time += time

    now = datetime.datetime.now()
    print(f"{now.strftime('%Y-%m-%d %H:%M:%S')}: Name={name}, Time={time}, Success={cost >= 0}, Cost={cost}")
    res.write(f"{name};{time};{cost >= 0};{cost}\n")


args = parse_params()
csv_file_name = f"{args.output_dir}/measurements.csv"
agrg_res_name = f"{args.output_dir}/aggregated.csv"

with open(csv_file_name, "w") as res:
    res.write("name;time;success;cost\n")
    for in_file in args.inputs:
        execute_process(args, in_file, res)

avg_time = total_succ_time / total_success if total_success > 0 else 0.0
succ_rate = total_success / len(args.inputs)

print("=====")
print("Aggregated result:")
print(f"Total: {len(args.inputs)}")
print(f"Success: {total_success}")
print(f"Success rate: {succ_rate}")
print(f"Average success time: {avg_time}")

with open(agrg_res_name, "w") as agrg:
    agrg.write("total;success;success_rate;avg_time\n")
    agrg.write(f"{len(args.inputs)};{total_success};{succ_rate};{avg_time}")