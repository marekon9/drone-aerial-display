# Drone Aerial Display

This project is a part of a diploma thesis that uses MAPF (Multi-agent Path Finding) algorithm for navigating drones in space where the drones are given positions in 3D space for each frame of the aerial display.

## Project Structure

The project consists of two programs. The first one is a command line application that takes display configuration as input and gives enhanced configuration with computed paths (this can be run in the visualising program). The latter one is a visualising tool that enables users to configure the aerial display and to see a 3D simulation of the drones flying to form user defined structures.

### File Structure

The following tree shows the most important files and directories in the project.

* `algorithm` - contains C++ sources to the MAPF algorithm.
* `data` - contains sample input configurations to the programs.
* `godot-cpp` - is an external git submodule containing sources and API headers to the Godot engine.
* `scripts` - helper scripts, e.g., for generating random display instances.
* `visualisator` - contains sources for simulation program.
    * `bin` - contains binary outputs.
    * `material` - contains reusable resources for the Godot Engine.
    * `mesh` - contains reusable resources for the Godot Engine.
    * `native` - contains C++ sources for the visualising library (is a layer that connects the application to the path finding algorithm).
    * `scenes` - contains scenes (like main menu, the main area, etc.)
    * `scripts` - contains scripts in Godot Engine scripting language.
    * `styles` - contains reusable resources for the Godot Engine.
    * `textures` - contains textures
    * `SConstruct` - build file for visualising library
* `CMakeLists.txt` - build file for the MAPF algorithm, also configures visualisation project for development.

## Command line application

As stated in sections above the command line application which takes display configuration as input (this can be obtained from the visualising tool) and outputs the configuration enhanced with the found path.

### Compilation

Before we can proceed with the compilation, please make sure the following dependencies are satisfied.
* C++ compiler that supports C++17.
* CMake, version at least 3.17
* Make (for Linux build)

For Linux the compilation sequence is this one:
* `cmake . -DCMAKE_BUILD_TYPE=Release` - generates makefile in the current directory.
* `make display` - will compile the CMD application.

On Windows use these commands instead:
* `cmake .`
* `cmake --build . --config Release`

### Usage

The program reads an input from the standard input. Similarly, it outputs to the standard output.

It is possible to add a command line parameter `simplePrint` which will cause the program to print output in more human-readable format instead of a CFG file.

**Examples (on Linux):**
* `./display < data/display.cfg` will output a CFG configuration for data/display.cfg file.
* `./display simplePrint < data/display.cfg` will output the result for data/display.cfg in more human-friendly format.

## Visualising application aka AED Visualiser

AED Visualiser (Aerial Display Visualiser) is a tool for simulating the movement of drones in the display.

### Compilation

The development of the visualiser can be done using CMake (supported in CLion IDE), however the compilation itself is done using SCons build system.

The prerequisites are as follows:
* C++ compiler that supports C++17
* SConstruct
* Godot 3.2. (from https://godotengine.org/)

In the following sections a placeholder `<distribution>` stands for one of the followings: `windows`, `linux`, `osx`.

Firstly the godot sources need to be initialized and compiled. This has to be done only once.
1) Type `git submodule update --init --recursive` to download sources of the Godot Engine.
2) Go to `godot-cpp` directory.
3) Type `scons target=release platform=<distribution>` to compile Godot sources.

Secondly we proceed with compiling of the visualiser library.
1) Go to `visualisator` directory.
2) Type command `scons target=release patform=<distribution>` to compile the library.

Finally, in order to generate an executable it is necessary to go to the Godot UI.
1) Open the project (the `visualisator` directory) in Godot.
2) Go to **Project** -> **Export...**
3) Click on **Add...** and select an appropriate target platform. Download the export template if necessary. This step has to be done only for the first time.
4) Select the appropriate platform in the presets.
5) In the right menu check section `Binary format`. Select the appropriate binary format (64 bit vs 32 bit). The recommended setup is also to set on the value of `Embed Pck` - this will embed program resources into the executable.
6) Click on the button **Export Project**.
7) When saving, the option `Export With Debug` should be unchecked.
8) Click on **Save**.
