#ifndef DISPLAY_AERIAL_DISPLAY_H
#define DISPLAY_AERIAL_DISPLAY_H

#include <vector>
#include <unordered_set>
#include <functional>

#include "graph.h"
#include "cbs.h"
#include "robot_paths.h"
#include "frame.h"
#include "aerial_display_paths.h"

namespace aed {
    class aerial_display {
    private:
        class graph graph;
        class cbs cbs;

        std::unique_ptr<aerial_frame_paths> transform(robot_paths * paths, const std::vector<color> & current_colors, const std::vector<color> & future_colors);

        void batch_drones(std::vector<destination> & destinations, const std::function<bool(const destination &, const destination &)>& cmp);

        static void determine_drone_positions(
                std::vector<destination> & current_destination,
                size_t count, std::vector<int> &current_ordering, std::unordered_set<int> &undetermined_drones,
                const frame & prev_frame,
                const frame &current_frame) ;
    public:

        aerial_display(const class graph& graph);

        std::vector<std::unique_ptr<aerial_frame_paths>> display_frames(const std::vector<frame> & frames);
    };
}

#endif //DISPLAY_AERIAL_DISPLAY_H
