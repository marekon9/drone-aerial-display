#ifndef DISPLAY_ROBOT_PATHS_H
#define DISPLAY_ROBOT_PATHS_H

#include <memory>
#include <vector>
#include <unordered_set>
#include "path_result.h"
#include "conflict_resolver.h"

namespace aed {
    struct agent {
        std::unique_ptr<path_result> path;
        conflict_resolver conflicts;

        agent(std::unique_ptr<path_result> path, conflict_resolver conflicts);

        agent(const agent & oth);

        agent(agent && oth) = default;
    };

    struct robot_paths {
        double total_cost;
        std::vector<agent> agents;

        robot_paths(double total_cost, std::vector<agent> agents);
    };

    std::ostream& operator << (std::ostream & os, const robot_paths & robot_paths);
}


#endif //DISPLAY_ROBOT_PATHS_H
