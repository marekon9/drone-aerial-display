#ifndef DISPLAY_CBS_H
#define DISPLAY_CBS_H

#include <memory>
#include "graph.h"
#include "robot_paths.h"
#include "a_star.h"
#include "destination.h"

namespace aed {

    class cbs {
    private:
        struct graph graph;
        class a_star a_star;

        std::shared_ptr<aed::robot_paths>
        initial_solution(const std::vector<aed::destination> &destinations);

        struct path_validation;
        std::unique_ptr<path_validation> validate_paths(const robot_paths& paths);

        bool handle_conflict_agent(aed::robot_paths *path,
                                   const std::unordered_set<int> &agents,
                                   const std::vector<aed::destination> &destinations, path_validation &validation);

    public:
        explicit cbs(const aed::graph & graph);

        std::shared_ptr<robot_paths> find_paths(const std::vector<destination> & destinations);

    };
}


#endif //DISPLAY_CBS_H
