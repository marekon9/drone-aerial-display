#include "aerial_display.h"
#include "matrix.h"
#include "hungarian_method.h"
#include <algorithm>
#include <memory>

aed::aerial_display::aerial_display(const aed::graph& graph) : graph(graph), cbs(graph) {}

std::vector<std::unique_ptr<aed::aerial_frame_paths>> aed::aerial_display::display_frames(const std::vector<aed::frame> &frames) {
    const frame & first_frame = frames[0];
    size_t count = first_frame.positions.size();

    std::vector<destination> current_destinations(count);
    std::vector<destination> future_destination(count);
    std::vector<color> current_colors(count);
    std::vector<color> future_colors(count);

    std::vector<int> current_ordering(count);

    for (int j = 0; j < count; j++) {
        int agent = first_frame.positions[j].agent;
        current_destinations[agent].from = first_frame.positions[j].position;

        current_ordering[j] = j;
        current_colors[agent] = first_frame.positions[j].color;
    }

    std::unordered_set<int> undetermined_positions(count);

    std::vector<std::unique_ptr<aerial_frame_paths>> paths;
    paths.reserve(frames.size() - 1);

    for (size_t i = 1; i < frames.size(); i++) {
        const frame & prev_frame = frames[i - 1];
        const frame & current_frame = frames[i];

        determine_drone_positions(current_destinations, count, current_ordering, undetermined_positions,
                                                     prev_frame, current_frame);

        for (int k = 0; k < count; k++) {
            int val = current_ordering[k];

            const auto & coordinates = current_frame.positions[k].position;
            current_destinations[val].to = coordinates;
            future_destination[val].from = coordinates;

            future_colors[val] = current_frame.positions[k].color;
        }

        if (i == 1 && graph.getBatches() > 0) {
            batch_drones(current_destinations, [] (const destination & a, const destination & b) {
                return a.to > b.to;});
        }

        if (i == frames.size() - 1 && graph.getBatches() > 0) {
            batch_drones(current_destinations, [] (const destination & a, const destination & b) {
                return a.to < b.to;});
        }

        auto path = cbs.find_paths(current_destinations);
        paths.push_back(transform(path.get(), current_colors, future_colors));

        std::swap(future_destination, current_destinations);
        std::swap(current_colors, future_colors);
    }

    return paths;
}

void aed::aerial_display::determine_drone_positions(
        std::vector<destination> & current_positions,
        size_t count, std::vector<int> &current_ordering, std::unordered_set<int> &undetermined_drones,
        const aed::frame & prev_frame,
        const aed::frame &current_frame) {
    undetermined_drones.clear();
    std::vector<int> undetermined_pos;

    for (int i = 0; i < count; ++i) {
        undetermined_drones.insert(i);
    }

    for (int j = 0; j < count; ++j) {
        int agent = current_frame.positions[j].agent;
        if (agent > 0) {
            undetermined_drones.erase(agent);
        } else {
            undetermined_pos.push_back(j);
        }
        current_ordering[j] = agent;
    }

    if (!undetermined_drones.empty()) {
        std::vector<int> tmp;
        tmp.reserve(undetermined_drones.size());

        matrix matrix((int) undetermined_drones.size());
        int row = 0;
        for (int droneIdx : undetermined_drones) {
            tmp.push_back(droneIdx);
            const auto & from = current_positions[droneIdx].from;

            int col = 0;
            for (int pos : undetermined_pos) {
                const auto & to = current_frame.positions[pos].position;
                matrix.set(row, col, from.distance(to));
                col++;
            }

            row++;
        }

        hungarian_method method;
        std::vector<int> computed_pos = method.compute_las(matrix);

        size_t idx = 0;
        for (size_t j = 0; j < count; j++) {
            if (current_ordering[j] < 0) {
                current_ordering[j] = tmp[computed_pos[idx++]];
            }
        }
    }
}

std::unique_ptr<aed::aerial_frame_paths> aed::aerial_display::transform(aed::robot_paths *paths, const std::vector<color> & current, const std::vector<color> & future) {
    if (paths == nullptr) {
        return nullptr;
    }

    std::vector<aerial_frame_drone> drone_paths;
    drone_paths.reserve(paths->agents.size());

    double max_cost = 0;

    size_t i = 0;
    for (auto & agent : paths->agents) {
        max_cost = std::max(max_cost, agent.path->cost);
        drone_paths.push_back(aerial_frame_drone{std::unique_ptr(std::move(agent.path)), current[i], future[i]});
        i++;
    }
    return std::make_unique<aerial_frame_paths>(aerial_frame_paths{paths->total_cost, max_cost, std::vector(std::move(drone_paths))});
}

void aed::aerial_display::batch_drones(std::vector<destination> &destinations,
                                       const std::function<bool(const destination &, const destination &)>& cmp) {
    std::set<destination, decltype(cmp)> sorted_destinations(cmp);
    std::set<destination, decltype(cmp)> result_destinations(cmp);

    std::vector<destination> batchDest;

    for (const auto & dest: destinations) {
        sorted_destinations.insert(dest);
    }

    int i = 0;
    while (!sorted_destinations.empty()) {
       int batchCnt = 0;

       auto it = sorted_destinations.begin();

       while (it != sorted_destinations.end() && batchCnt < graph.getBatches()) {
           bool insert = true;

           for (const auto & batched: batchDest) {
                if (batched.from.distance(it->from) < graph.getBatchDistance() || batched.to.distance(it->to) < graph.getBatchDistance()) {
                    insert = false;
                    break;
                }
           }

           if (insert) {
               batchDest.push_back(*it);
               batchDest.rbegin()->wait_round = i;
               result_destinations.insert(*batchDest.rbegin());
               batchCnt++;
           }

           it++;
       }

       for (const auto & batchElement: batchDest) {
            sorted_destinations.erase(batchElement);
       }
       batchDest.clear();
       i++;
    }

    for (auto & dest: destinations) {
        dest.wait_round = result_destinations.find(dest)->wait_round;
    }
}


