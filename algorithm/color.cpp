#include "color.h"

std::ostream & aed::operator<<(std::ostream &os, const struct color &color) {
    os << "[" << int(color.red) << ", " << int(color.green) << ", " << int (color.blue) << ", " << color.active <<"]";
    return os;
}