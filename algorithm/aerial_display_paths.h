#ifndef DISPLAY_AERIAL_DISPLAY_PATHS_H
#define DISPLAY_AERIAL_DISPLAY_PATHS_H

#include <vector>
#include <memory>
#include "path_result.h"
#include "color.h"

namespace aed {
    struct aerial_frame_drone {
        std::unique_ptr<path_result> path;
        struct color from_color;
        struct color to_color;
    };

    struct aerial_frame_paths {
        double total_cost;
        double max_cost;
        std::vector<aerial_frame_drone> drones;
    };

    std::ostream & operator << (std::ostream & os, const aerial_frame_paths & frame_paths);
}

#endif //DISPLAY_AERIAL_DISPLAY_PATHS_H
