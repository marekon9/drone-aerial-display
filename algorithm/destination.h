#ifndef DISPLAY_DESTINATION_H
#define DISPLAY_DESTINATION_H

#include "coordinates.h"
#include <iostream>

namespace aed {
    struct destination {
        coordinates from;
        coordinates to;
        int wait_round;

        destination() : from(), to(), wait_round(0) {}

        destination(const coordinates &from, const coordinates &to, int wait_round) : from(from), to(to), wait_round(wait_round) {}
    };

    std::ostream & operator << (std::ostream & os, const destination & dest);
}

#endif //DISPLAY_DESTINATION_H
