#ifndef DISPLAY_COLOR_H
#define DISPLAY_COLOR_H

#include <iostream>

namespace aed {
    struct color {
        unsigned char red;
        unsigned char green;
        unsigned char blue;
        bool active;
    };

    std::ostream & operator << (std::ostream & os, const struct color & color);
}


#endif //DISPLAY_COLOR_H
