#include "cbs.h"
#include "a_star.h"
#include "conflict_resolver.h"

#include <utility>
#include <queue>
#include <unordered_map>
#include <iostream>

struct aed::cbs::path_validation {
    int first_agent;
    std::unordered_set<int> others;
    std::vector<box_occupation> conflicts;

    path_validation(int firstAgent, std::unordered_set<int> && others, std::vector<box_occupation> && conflicts)
            : first_agent(firstAgent), others(std::move(others)), conflicts(std::move(conflicts)) {}
};

aed::cbs::cbs(const aed::graph & graph) : graph(graph), a_star(graph) {
}

std::shared_ptr<aed::robot_paths> aed::cbs::find_paths(const std::vector<destination> & destinations) {
    auto init_solution = initial_solution(destinations);
    if (init_solution == nullptr) {
        return nullptr;
    }

    auto comp = [] (const std::shared_ptr<aed::robot_paths> & a, const std::shared_ptr<aed::robot_paths> & b) {
        return a->total_cost > b->total_cost;
    };

    std::priority_queue<std::shared_ptr<aed::robot_paths>, std::vector<std::shared_ptr<aed::robot_paths>>, decltype(comp)> queue(comp);
    queue.push(init_solution);

    while (!queue.empty()) {
        auto path = queue.top();
        queue.pop();

        auto validation_res = validate_paths(*path);
        if (validation_res == nullptr) {
            return path;
        }

        auto copy = std::make_shared<robot_paths>(*path);
        if (handle_conflict_agent(path.get(), {validation_res->first_agent}, destinations, *validation_res)) {
            queue.push(path);
        }

        if (handle_conflict_agent(copy.get(), validation_res->others, destinations, *validation_res)) {
            queue.push(copy);
        }
    }

    return nullptr;
}

bool aed::cbs::handle_conflict_agent(aed::robot_paths *path,
                                     const std::unordered_set<int> &agents,
                                     const std::vector<aed::destination> &destinations, path_validation &validation) {
    for (int agent_pos : agents) {
        aed::agent & agent = path->agents[agent_pos];
        for (const auto & conflict : validation.conflicts) {
            if (agent.path->cost < conflict.time.from || (conflict.time.to == std::numeric_limits<double>::infinity() &&
                                                          destinations[agent_pos].to == conflict.box)) {
                return false;
            }
            agent.conflicts.add_conflict(conflict);
        }

        auto new_path = a_star.find_path(destinations[agent_pos].wait_round, agent.conflicts, destinations[agent_pos].from, destinations[agent_pos].to);

        if (new_path != nullptr) {
            path->total_cost = path->total_cost - agent.path->cost + new_path->cost;
            agent.path->path = std::move(new_path->path);
            agent.path->cost = new_path->cost;
        } else {
            return false;
        }
    }
    return true;
}

std::shared_ptr<aed::robot_paths>
aed::cbs::initial_solution(const std::vector<aed::destination> &destinations) {
    double sum = 0;
    std::vector<agent> agents;
    agents.reserve(destinations.size());

    for (const auto & destination : destinations) {
        conflict_resolver conflicts;

        auto path = a_star.find_path(destination.wait_round, conflicts, destination.from, destination.to);

        if (path == nullptr) {
            return nullptr;
        }

        sum += path->cost;
        agents.emplace_back(std::move(path), conflicts);
    }

    return std::make_shared<aed::robot_paths>(sum, agents);
}

namespace aed {
    struct agent_timing {
        int agent;
        double time_to;

        agent_timing(int agent, double timeTo) : agent(agent), time_to(timeTo) {}
    };
}

std::unique_ptr<aed::cbs::path_validation> aed::cbs::validate_paths(const robot_paths &paths) {
    std::unordered_map<coordinates, std::map<double, agent_timing>> conflict_map;
    std::unordered_set<int> agents;
    std::vector<box_occupation> conflicts;
    std::vector<box_occupation> trajectory;

    int i = 0;
    for (const auto & agent : paths.agents) {
        path_result_node prev = agent.path->path[0];
        for (size_t j = 1; j < agent.path->path.size(); j++) {
            const auto & current = agent.path->path[j];
            coordinates next_dir;
            if (j <= agent.path->path.size() - 2) {
                path_result_node &next_node = agent.path->path[j + 1];
                if (next_node.speed > 0) {
                    next_dir = next_node.vertex - current.vertex;
                }
            }

            if (prev.vertex != current.vertex) {
                neighbour neigh(current.vertex - prev.vertex, current.length, next_dir, current.t1, current.t2, current.t3);
                graph.find_trajectory(trajectory, neigh, prev.speed, current.total_time - prev.total_time,
                                      current.start_acceleration, current.end_acceleration);
                for (const auto & box : trajectory) {

                    auto & box_conflict_map = conflict_map[box.box + prev.vertex];

                    // TODO compute ellipsoid collisions instead

                    double lower_bound = box.time.from + prev.total_time;
                    double upper_bound;

                    auto it = box_conflict_map.lower_bound(lower_bound);
                    bool conflict = false;

                    while (it != box_conflict_map.end()) {
                        if (it->first > box.time.to + prev.total_time) {
                            break;
                        }
                        conflict = true;
                        upper_bound = it->second.time_to;
                        agents.insert(it->second.agent);
                        it++;
                    }

                    if (!conflict) {
                        box_conflict_map.insert(std::pair(lower_bound, agent_timing(i, prev.total_time + box.time.to)));
                    } else {
                        conflicts.push_back({time_interval(lower_bound, upper_bound), box.distance, box.box + prev.vertex});
                    }
                }

                if (!conflicts.empty()) {
                    return std::make_unique<aed::cbs::path_validation>(i, std::move(agents), std::move(conflicts));
                }
            } else {
                if (current.total_time == 0) {
                    continue;
                }

                auto & box_conflict_map = conflict_map[current.vertex];
                box_conflict_map.insert(std::pair(prev.total_time, agent_timing(i, current.total_time)));
            }
            prev = current;
        }

        auto & box_conflict_map = conflict_map[prev.vertex];
        box_conflict_map.insert(std::pair(prev.total_time, agent_timing(i, std::numeric_limits<double>::infinity())));

        i++;
    }

    return nullptr;
}
