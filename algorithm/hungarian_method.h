#ifndef DISPLAY_HUNGARIAN_METHOD_H
#define DISPLAY_HUNGARIAN_METHOD_H

#include "matrix.h"
#include <vector>
#include <unordered_set>

namespace aed {
    class hungarian_method {
    private:
        bool equals(double a, double b);

        template<typename T> std::vector<T> initiate_vector(int size, T value);

        void preprocessing(const matrix & orig, matrix & cost_red, std::vector<double> & row_red, std::vector<double> & col_red, std::vector<int> & row_ass, std::vector<int> & col_ass);

        int augment(int n, int row_vertex, const matrix & orig, std::vector<double> & row_red, std::vector<double> & col_red, std::vector<int> & row_ass, std::vector<int> & predecessors);
    public:

        std::vector<int> compute_las(const matrix & orig);


    };
}


#endif //DISPLAY_HUNGARIAN_METHOD_H
