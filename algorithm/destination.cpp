#include "destination.h"

std::ostream& aed::operator<<(std::ostream& os, const destination& dest) {
    os << dest.from << " -> " << dest.to;
    return os;
}