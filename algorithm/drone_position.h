#ifndef DISPLAY_DRONE_POSITION_H
#define DISPLAY_DRONE_POSITION_H

#include "coordinates.h"
#include "color.h"

namespace aed {
    struct drone_position {
        coordinates position;
        struct color color;
        int agent;

        drone_position(const coordinates &position, const struct color &color) : position(position), color(color), agent(-1) {}

        drone_position(const coordinates &position, const struct color &color, int agent) : position(position), color(color),
                                                                                     agent(agent) {}
    };
}
#endif //DISPLAY_DRONE_POSITION_H
