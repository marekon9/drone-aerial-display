#ifndef DISPLAY_BOX_OCCUPATION_H
#define DISPLAY_BOX_OCCUPATION_H

#include "time_interval.h"
#include "coordinates.h"

namespace aed {
    struct box_occupation {
        time_interval time;
        double distance;
        coordinates box;

        bool operator==(const box_occupation &oth) const noexcept;

        bool operator!=(const box_occupation &oth) const noexcept;
    };
}
#endif //DISPLAY_BOX_OCCUPATION_H
