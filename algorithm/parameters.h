#ifndef DISPLAY_PARAMETERS_H
#define DISPLAY_PARAMETERS_H

#include <iostream>
#include <vector>
#include "graph.h"
#include "frame.h"
#include "path_result.h"
#include "aerial_display_paths.h"

namespace aed {
    class parameters {
        struct graph graph;
        int drone_count;
        double wait_time;
        std::vector<frame> frames;
        std::vector<std::unique_ptr<aerial_frame_paths>> frame_paths;

        void validate_parameters();

        void validate_graph();

        void validate_frames();

        void validate_path();

        enum type {
            ELEMENT_BEGIN, ELEMENT_END, PAIR, READ_EOF, READ_FAILURE
        };

        struct parsed_row {
            enum type type;
            std::string key;
            std::string value;
        };
        static parsed_row parse_row(char * buffer, int & line, std::istream & is);

        static void parse_graph(struct graph & graph, char * buffer, int & line, std::istream & is);

        static void parse_display(std::vector<frame> & frames, int & drone_count, double & wait_time, char * buffer, int & line, std::istream & is);

        static void parse_frames(std::vector<frame> & frames, char * buffer, int & line, std::istream & is);

        static void parse_frame(std::vector<frame> & frames, char * buffer, int & line, std::istream & is);

        static void parse_drone_positions(std::vector<drone_position> & position, char * buffer, int & line, std::istream & is);

        static void parse_color(color & color, char * buffer, int & line, std::istream & is, const std::string& element = "color");

        static void parse_coordinates(coordinates & coordinates, char * buffer, int & line, std::istream & is, const std::string & element = "coordinates");

        static void parse_double_vector(double_vector & vector, char * buffer, int & line, std::istream & is, const std::string & element);

        static void parse_aed_frame_paths(std::vector<std::unique_ptr<aerial_frame_paths>> & frame_paths, char * buffer, int & line, std::istream & is);

        static void parse_aed_frame(std::vector<std::unique_ptr<aerial_frame_paths>> & frame_paths, char * buffer, int & line, std::istream & is);

        static void parse_aed_frame_missing(std::vector<std::unique_ptr<aerial_frame_paths>> & frame_paths, char * buffer, int & line, std::istream & is);

        static void parse_aed_frame_drones(std::vector<aerial_frame_drone> & frame_drone, char * buffer, int & line, std::istream & is);

        static void parse_aed_frame_drone(std::vector<aerial_frame_drone> & frame_drone, char * buffer, int & line, std::istream & is);

        static std::unique_ptr<path_result> parse_path(char * buffer, int & line, std::istream & is);

        static void parse_path_frame_result(std::vector<path_result_node> & path, char * buffer, int & line, std::istream & is);

        static void parse_path_frame_result_node(std::vector<path_result_node> & path, char * buffer, int & line, std::istream & is);

        template<class T> static void validate_property(std::vector<std::string> & props, const std::string & name, T value);

        template<typename T> static void validate_non_negative_property(std::vector<std::string> &props, const std::string &name, T value);

        static void validate_bool_property(std::vector<std::string> & props, const std::string & name, bool initialized);

        static void validate_props(int line, char * buffer, std::vector<std::string> & props);

    public:
        parameters(const struct graph & graph, int drone_count, double wait_time, const std::vector<frame> & frames, std::vector<std::unique_ptr<aerial_frame_paths>> && frame_paths);

        parameters(struct graph && graph, int drone_count, double wait_time, std::vector<frame> && frames, std::vector<std::unique_ptr<aerial_frame_paths>> && frame_paths);

        parameters(std::istream & is);

        void save(std::ostream & os, bool include_results) const;

        const struct graph & get_graph() const noexcept;

        int get_drone_count() const noexcept;

        double get_wait_time() const noexcept;

        const std::vector<frame> & get_frames() const noexcept;

        const std::vector<std::unique_ptr<aerial_frame_paths>> & get_frame_paths() const noexcept;

        void set_frame_paths(std::vector<std::unique_ptr<aerial_frame_paths>> &&paths);

    };
}


#endif //DISPLAY_PARAMETERS_H
