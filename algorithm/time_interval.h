#ifndef DISPLAY_TIME_INTERVAL_H
#define DISPLAY_TIME_INTERVAL_H

namespace aed {
    struct time_interval {
        double from;
        double to;

        time_interval() = default;

        time_interval(double from, double to);

        bool operator == (const time_interval & oth) const noexcept;
    };
}

#endif //DISPLAY_TIME_INTERVAL_H
