#include "time_interval.h"

aed::time_interval::time_interval(double from, double to) : from(from), to(to) {}

bool aed::time_interval::operator==(const aed::time_interval &oth) const noexcept {
    return from == oth.from && to == oth.to;
}
