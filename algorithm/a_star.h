#ifndef DISPLAY_A_STAR_H
#define DISPLAY_A_STAR_H

#include <vector>
#include <memory>
#include <queue>
#include "coordinates.h"
#include "graph.h"
#include "path_result.h"
#include "conflict_resolver.h"

namespace aed {

    class a_star {
    private:
        graph _graph;

        struct path_node;

        static double compute_cross_acceleration_time(double acc1, double acc2, double init_speed, double final_speed, double total_distance);

        template<typename T, bool stop>
        std::unique_ptr<aed::path_result> iterate_over_neighbors(const aed::conflict_resolver &conflicts,
                                                                 std::priority_queue<std::shared_ptr<aed::a_star::path_node>, std::vector<std::shared_ptr<aed::a_star::path_node>>, T> &queue,
                                                                 std::unordered_set<aed::coordinates> &visited_at_the_moment,
                                                                 std::vector<box_occupation> &trajectory,
                                                                 const std::vector<neighbour> &neighbors,
                                                                 std::unordered_set<aed::coordinates> &visited,
                                                                 const std::shared_ptr<aed::a_star::path_node> &element_ptr,
                                                                 const aed::coordinates &from,
                                                                 const aed::coordinates &to,
                                                                 std::unordered_map<aed::coordinates, std::shared_ptr<a_star::path_node>> &wait_map);
    public:
        explicit a_star(graph  _graph);

        std::unique_ptr<path_result> find_path(int waitRound, const conflict_resolver & conflicts, const coordinates & from, const coordinates & to);
    };
}



#endif //DISPLAY_A_STAR_H
