#ifndef DISPLAY_COORDINATES_H
#define DISPLAY_COORDINATES_H

#include <ostream>

namespace aed {
    struct coordinates {
        int x;
        int y;
        int z;

        coordinates();

        coordinates(int x, int y, int z) noexcept;

        bool is_zero() const noexcept;

        coordinates operator + (const coordinates & oth) const noexcept;

        coordinates operator - (const coordinates & oth) const noexcept;

        bool operator < (const coordinates & oth) const noexcept;

        bool operator > (const coordinates & oth) const noexcept;

        bool operator == (const coordinates & oth) const noexcept;

        bool operator!=(const coordinates &oth) const noexcept;

        double distance(const coordinates & oth) const noexcept;
    };

    std::ostream & operator << (std::ostream & os, const coordinates & coord);
}

namespace std {
    template<> struct hash<aed::coordinates> {
        size_t operator()(const aed::coordinates & coordinates) const noexcept;
    };
}

#endif //DISPLAY_COORDINATES_H
