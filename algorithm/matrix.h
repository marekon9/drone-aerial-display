#ifndef DISPLAY_MATRIX_H
#define DISPLAY_MATRIX_H

#include <iostream>

namespace aed {
    class matrix {
    private:
        int n;
        double* data;
    public:
        matrix(int n);

        matrix(const matrix & oth);

        matrix(matrix && oth) noexcept ;

        int get_n() const noexcept;

        double get(int row, int col) const noexcept;

        void set(int row, int col, double value) noexcept;

        ~matrix();
    };

    std::ostream & operator << (std::ostream & os, const matrix & matrix);
}


#endif //DISPLAY_MATRIX_H
