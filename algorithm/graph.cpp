#include <cmath>
#include <map>
#include "graph.h"
#include "coordinates.h"

aed::graph::graph(int length, int width, int height, int k, int precision, double curvature, double space,
                  double elipsoid_a,
                  double elipsoid_b, double elipsoid_c, double max_speed, double max_acceleration, double stop_penalty,
                  double distance_multiplier,
                  int batches,
                  double batch_delay,
                  double batch_distance)
    : length(length), width(width), height(height), k(k), precision(precision), curvature(curvature),
    basic_case(get_init_neighbors(k, space)),
    neighborhood(),
    space(space),
    elipsoid_a(elipsoid_a), elipsoid_b(elipsoid_b), elipsoid_c(elipsoid_c),
    max_speed(max_speed), max_acceleration(max_acceleration), stop_penalty(stop_penalty),
    distance_multiplier(distance_multiplier),
    batches(batches),
    batch_delay(batch_delay),
    batch_distance(batch_distance) {
    all_init_neighbors();
}


const std::vector<aed::neighbour> &
aed::graph::get_neighborhood_in_direction(const aed::coordinates &direction) {
    return neighborhood[direction];
}

void aed::graph::all_init_neighbors() {
    for (const auto & direction : basic_case) {
        auto & neighbors = neighborhood[direction.coord];
        compute_neighborhood_in_direction(direction.coord, neighbors);
    }

    auto & zero_direction = neighborhood[coordinates()];
    for (const auto & it : neighborhood) {
        if (it.first.is_zero()) {
            continue;
        }
        for (const auto & neigh : it.second) {
            zero_direction.push_back(neigh);
        }
    }
}

void aed::graph::compute_neighborhood_in_direction(const coordinates &direction, std::vector<neighbour> &neighbors) {
    double c = curvature;
    double c2 = c * c;
    double c3 = c2 * c;

    double_vector direction_vector(direction.x * space, direction.y * space, direction.z * space);

    const auto & init = basic_case;
    for (const auto & element : init) {
        const auto & coord = element.coord;
        if (direction.x == -coord.x && direction.y == -coord.y && direction.z == -coord.z) {
            continue;
        }

        double_vector coord_vector(coord.x * space, coord.y * space, coord.z * space);

        double_vector t1;
        t1.x = direction_vector.x;
        t1.y = direction_vector.y;
        t1.z = direction_vector.z;

        double_vector t2;
        t2.x = 3 * direction_vector.x / c2 - (2 * direction_vector.x + coord_vector.x) / c;
        t2.y = 3 * direction_vector.y / c2 - (2 * direction_vector.y + coord_vector.y) / c;
        t2.z = 3 * direction_vector.z / c2 - (2 * direction_vector.z + coord_vector.z) / c;

        double_vector t3;
        t3.x = 2 * (-direction_vector.x) / c3 + (direction_vector.x + coord_vector.x) / c2;
        t3.y = 2 * (-direction_vector.y) / c3 + (direction_vector.y + coord_vector.y) / c2;
        t3.z = 2 * (-direction_vector.z) / c3 + (direction_vector.z + coord_vector.z) / c2;

        double path_length = 0;
        double_vector prev;
        double_vector current;

        for (int i = 1; i <= precision; ++i) {
            double ratio = ((double) i) / precision * c;
            double ratio2 = ratio * ratio;
            double ratio3 = ratio2 * ratio;
            current.x = t1.x * ratio + t2.x * ratio2 + t3.x * ratio3;
            current.y = t1.y * ratio + t2.y * ratio2 + t3.y * ratio3;
            current.z = t1.z * ratio + t2.z * ratio2 + t3.z * ratio3;

            path_length += prev.distance(current);

            prev = current;
        }

        neighbors.emplace_back(direction, path_length, element.coord, t1, t2, t3);
    }
}

const std::vector<aed::neighbour> &aed::graph::get_stop_neighborhood_in_direction(std::vector<neighbour> & neighbors, const aed::coordinates &direction) {
    if (direction.is_zero()) {
        return basic_case;
    }

    neighbors.clear();
    neighbors.emplace_back(direction, space * direction.distance({}));

    return neighbors;
}

std::vector<aed::neighbour> aed::graph::get_init_neighbors(int k, double space) {
    std::vector<neighbour> neighbourhood = compute_neighbourhood(k, space);

    int multipliers [] = {1, -1};
    std::vector<aed::neighbour> neighbors;

    for (int i_m : multipliers) {
        for (int j_m : multipliers) {
            for (int k_m : multipliers) {
                for (auto & neighbour : neighbourhood) {
                    int res_x = i_m * neighbour.coord.x;
                    int res_y = j_m * neighbour.coord.y;
                    int res_z = k_m * neighbour.coord.z;

                    if (is_duplicate_neighbour(neighbour.coord, i_m, j_m, k_m)) {
                        continue;
                    }

                    neighbors.emplace_back(coordinates(res_x, res_y, res_z), neighbour.length);
                }
            }
        }
    }

    return neighbors;
}

bool aed::graph::is_in_graph(const aed::coordinates &pos) const {
    return pos.x >= 0 && pos.x < length && pos.y >= 0 && pos.y < width && pos.z >= 0 && pos.z < height;
}

std::vector<aed::neighbour> aed::graph::compute_neighbourhood(int k, double space) {
    if (k <= 2) {
        return std::vector<aed::neighbour> {
            neighbour(coordinates(1, 0, 0), space),
            neighbour(coordinates(0, 1, 0), space),
            neighbour(coordinates(0, 0, 1), space)
        };
    }

    auto prev = compute_neighbourhood(k - 1, space);
    std::vector<aed::neighbour> res;
    std::unordered_set<aed::coordinates> orig;
    std::unordered_set<aed::coordinates> res_set;

    for (auto & item : prev) {
        res.push_back(item);
        orig.insert(item.coord);

        for (auto & item2 : prev) {
            res_set.insert(item.coord + item2.coord);
        }

    }

    for (auto & coord : res_set) {
        if (orig.find(coord) == orig.end()) {
            double distance = std::sqrt(space * space * (coord.x * coord.x + coord.y * coord.y + coord.z * coord.z));
            res.emplace_back(coord, distance);
        }
    }

    return res;
}

bool aed::graph::is_duplicate_neighbour(const coordinates &coord, int m_x, int m_y, int m_z) {
    return (coord.x == 0 && m_x < 0) || (coord.y == 0 && m_y < 0) || (coord.z == 0 && m_z < 0);
}

double dist(const aed::coordinates & to) {
    return sqrt(to.x * to.x + to.y * to.y + to.z * to.z);
}

double aed::graph::compute_cos(double coord_dist, int b, int c) {
    if (b == 0 && c == 0) {
        return 1;
    } else {
        return (b * b + c * c) / (coord_dist * dist(aed::coordinates(b, c, 0)));
    }
}


void aed::graph::find_trajectory(std::vector<box_occupation> &res, const aed::neighbour &neighbor, double init_speed,
                                 double time, const path_res_acceleration &acc, const path_res_acceleration &deacc) {
    // TODO cache :)
    clear_vectors(res);

    if (neighbor.continue_dir.is_zero() || neighbor.continue_dir == neighbor.coord) {
        linear_intervals(neighbor, init_speed, time, acc, deacc);
    } else {
        curved_intervals(neighbor, init_speed, time, acc, deacc);
    }

    for (auto & x_interval : x_intervals) {
        for (auto & y_interval : y_intervals) {
            for (auto & z_interval : z_intervals) {
                double from_time;
                double from_length;
                max_time_and_length(x_interval, y_interval, z_interval, from_time, from_length);

                double to_time = std::min(x_interval.time.to, std::min(y_interval.time.to, y_interval.time.to));

                if (from_time <= to_time) {
                    res.push_back({time_interval(from_time, time), from_length, coordinates(x_interval.axis_coordinate, y_interval.axis_coordinate, z_interval.axis_coordinate)});
                }

            }
        }
    }

}

void
aed::graph::curved_intervals(const aed::neighbour &neighbor, double init_speed, double time,
                             const path_res_acceleration &acc, const path_res_acceleration &deacc) {
    double path_length = 0;
    double middle = space / 2;
    double_vector prev_right(middle + elipsoid_a, middle + elipsoid_b, middle + elipsoid_c);
    coordinates prev_right_quadrants;
    coordinates prev_left_quadrants;

    double_vector current_right;
    coordinates current_right_quadrants;
    double_vector current_left;
    coordinates current_left_quadrants;

    for (int i = 1; i <= precision; ++i) {
        double ratio = ((double) i) / precision * curvature;

        compute_current_quadrant(path_length, ratio, elipsoid_a, neighbor.t.x, neighbor.t2.x, neighbor.t3.x,
                                 current_left.x, current_left_quadrants.x, prev_left_quadrants.x, lefts_x,
                                 current_right.x, current_right_quadrants.x, prev_right_quadrants.x, rights_x);
        compute_current_quadrant(path_length, ratio, elipsoid_b, neighbor.t.y, neighbor.t2.y, neighbor.t3.y,
                                 current_left.y, current_left_quadrants.y, prev_left_quadrants.y, lefts_y,
                                 current_right.y, current_right_quadrants.y, prev_right_quadrants.y, rights_y);
        compute_current_quadrant(path_length, ratio, elipsoid_c, neighbor.t.z, neighbor.t2.z, neighbor.t3.z,
                                 current_left.z, current_left_quadrants.z, prev_left_quadrants.z, lefts_z,
                                 current_right.z, current_right_quadrants.z, prev_right_quadrants.z, rights_z);

        path_length += current_right.distance(prev_right);

        prev_right = current_right;
        prev_right_quadrants = current_right_quadrants;
        prev_left_quadrants = current_left_quadrants;
    }

    make_axis_intervals(neighbor, init_speed, acc, deacc, x_intervals, rights_x, lefts_x);
    make_axis_intervals(neighbor, init_speed, acc, deacc, y_intervals, rights_y, lefts_y);
    make_axis_intervals(neighbor, init_speed, acc, deacc, z_intervals, rights_z, lefts_z);
}

void aed::graph::linear_intervals(const aed::neighbour &neighbor, double init_speed, double time,
                                  const path_res_acceleration &acc, const path_res_acceleration &deacc) {

    coordinates normalized_to(std::abs(neighbor.coord.x), std::abs(neighbor.coord.y), std::abs(neighbor.coord.z));

    double coord_dist = normalized_to.distance({});

    double cos_alpha_x = compute_cos(coord_dist, normalized_to.y, normalized_to.z);
    double cos_alpha_y = compute_cos(coord_dist, normalized_to.x, normalized_to.z);
    double cos_alpha_z = compute_cos(coord_dist, normalized_to.x, normalized_to.y);

    coord_dist *= space;

    linear_durations(x_intervals, coord_dist, elipsoid_a, neighbor.coord.x, cos_alpha_x, time, init_speed, acc, deacc);
    linear_durations(y_intervals, coord_dist, elipsoid_b, neighbor.coord.y, cos_alpha_y, time, init_speed, acc, deacc);
    linear_durations(z_intervals, coord_dist, elipsoid_c, neighbor.coord.z, cos_alpha_z, time, init_speed, acc, deacc);
}

void
aed::graph::linear_durations(std::vector<axis_occupation> &intervals, double max_distance, double drone_size, int dist,
                             double cos, double time, double init_speed, const path_res_acceleration &acc,
                             const path_res_acceleration &deacc) {
    if (dist == 0) {
        intervals.push_back({time_interval(0,time), 0, 0});
        return;
    }

    double middle = space / 2;

    double from_start = middle + drone_size;
    double to_start = middle - drone_size;

    intervals.reserve(std::abs(dist) + 1);

    for (int i = 0; i <= std::abs(dist); ++i) {
        double from = std::max(0.0, (i * space - from_start) / cos);
        double to = std::min(max_distance, ((i + 1) * space - to_start) / cos);

        double from_time = length_to_time(from, max_distance, init_speed, acc, deacc);
        double to_time = length_to_time(to, max_distance, init_speed, acc, deacc);

        intervals.push_back({time_interval(from_time, to_time), from, i * sgn(dist)});
    }
}

void aed::graph::clear_vectors(std::vector<box_occupation> &res) {
    res.clear();
    x_intervals.clear();
    y_intervals.clear();
    z_intervals.clear();
    lefts_x.clear();
    rights_x.clear();
    lefts_y.clear();
    rights_y.clear();
    lefts_z.clear();
    rights_z.clear();
}

void
aed::graph::compute_current_quadrant(double path_length, double ratio, double drone_size, double t1, double t2,
                                     double t3, double &left,
                                     int &left_quadrant,
                                     int prev_left_quadrant,
                                     std::vector<std::pair<double, int>> &lefts, double &right,
                                     int &right_quadrant, int prev_right_quadrant,
                                     std::vector<std::pair<double, int>> &rights) {
    double ratio2 = ratio * ratio;
    double ratio3 = ratio2 * ratio;
    double middle = space/2;
    double middle_pos = t1 * ratio + t2 * ratio2 + t3 * ratio3 + middle;
    left = middle_pos - drone_size;
    left_quadrant = (int) std::floor(left / space);
    right = middle_pos + drone_size;
    right_quadrant = (int) std::floor(right / space);

    // so we avoid tricky situations at the end when the radius is the same as half of the grid size
    bool not_at_end = std::abs(ratio - curvature) > std::numeric_limits<double>::epsilon();
    if (left_quadrant != prev_left_quadrant && (not_at_end || left_quadrant > prev_left_quadrant )) {
        lefts.emplace_back(path_length, left_quadrant);
    }

    if (right_quadrant != prev_right_quadrant && (not_at_end || right_quadrant < prev_right_quadrant)) {
        rights.emplace_back(path_length, right_quadrant);
    }
}

std::vector<aed::axis_occupation> &
aed::graph::make_axis_intervals(const aed::neighbour &neighbor, double init_speed, const path_res_acceleration &acc,
                                const path_res_acceleration &deacc, std::vector<axis_occupation> &intervals,
                                const std::vector<std::pair<double, int>> &rights_x,
                                const std::vector<std::pair<double, int>> &lefts_x) const {
    enum MOV_TYPE {
        LEFT, SAME, RIGHT
    };

    MOV_TYPE movement_type = SAME;

    size_t left_idx = 0;
    size_t right_idx = 0;
    size_t stable_idx = 0;
    size_t i = 0;

    intervals.push_back({time_interval(0, neighbor.length), 0, 0});

    while (!(left_idx >= lefts_x.size() && right_idx >= rights_x.size())) {
        double left_length = left_idx >= lefts_x.size() ? neighbor.length : lefts_x[left_idx].first;
        double right_length = right_idx >= rights_x.size() ? neighbor.length : rights_x[right_idx].first;

        if (movement_type == SAME) {
            if (left_length < right_length) {
                intervals.push_back({time_interval(left_length, neighbor.length), left_length, lefts_x[left_idx++].second});
                i++;
                movement_type = LEFT;
            } else {
                intervals.push_back({time_interval(right_length, neighbor.length), right_length, rights_x[right_idx++].second});
                i++;
                movement_type = RIGHT;
            }
        } else if (movement_type == LEFT) {
            if (left_length < right_length) {
                intervals[i].time.to = left_length;
                movement_type = SAME;
                left_idx++;
            } else {
                intervals[stable_idx].time.to = right_length;
                movement_type = SAME;
                stable_idx = i;
                right_idx++;
            }
        } else {
            if (left_length <= right_length) {
                intervals[stable_idx].time.to = left_length;
                movement_type = SAME;
                stable_idx = i;
                left_idx++;
            } else {
                intervals[i].time.to = right_length;
                movement_type = SAME;
                right_idx++;
            }
        }
    }

    // transform lengths to time intervals
    for (auto & interval : intervals) {
        interval.time.from = length_to_time(interval.time.from, neighbor.length, init_speed, acc, deacc);
        interval.time.to = length_to_time(interval.time.to, neighbor.length, init_speed, acc, deacc);
    }
    return intervals;
}

void aed::graph::max_time_and_length(const aed::axis_occupation &x_interval, const aed::axis_occupation &y_interval,
                                     const aed::axis_occupation &z_interval, double &from_time,
                                     double &from_length) {
    if (x_interval.time.from >= y_interval.time.from) {
        if (x_interval.time.from >= z_interval.time.from) {
            from_time = x_interval.time.from;
            from_length = x_interval.from_length;
        } else {
            from_time = z_interval.time.from;
            from_length = z_interval.from_length;
        }
    } else {
        if (y_interval.time.from >= z_interval.time.from) {
            from_time = y_interval.time.from;
            from_length = y_interval.from_length;
        } else {
            from_time = z_interval.time.from;
            from_length = z_interval.from_length;
        }
    }
}

int aed::graph::sgn(int a) noexcept {
    return a > 0 ? 1 : -1;
}

int aed::graph::getLength() const {
    return length;
}

int aed::graph::getWidth() const {
    return width;
}

int aed::graph::getHeight() const {
    return height;
}

int aed::graph::getK() const {
    return k;
}

double aed::graph::getSpace() const {
    return space;
}

double aed::graph::getElipsoidA() const {
    return elipsoid_a;
}

double aed::graph::getElipsoidB() const {
    return elipsoid_b;
}

double aed::graph::getElipsoidC() const {
    return elipsoid_c;
}

int aed::graph::getPrecision() const {
    return precision;
}

double aed::graph::getCurvature() const {
    return curvature;
}

double aed::graph::getMaxSpeed() const {
    return max_speed;
}

double aed::graph::getMaxAcceleration() const {
    return max_acceleration;
}

double
aed::graph::length_to_time(double length, double total_length, double init_speed, const path_res_acceleration &acc,
                           const path_res_acceleration &deacc) {
    double acc_length = (acc.duration * acc.duration * acc.acceleration + 2 * init_speed * acc.duration) / 2;

    if (length <= acc_length) {
        double discriminant = std::sqrt(4 * init_speed * init_speed + 8 * acc.acceleration * length);
        double res = std::max((-2 * init_speed + discriminant) / (2 * acc.acceleration),
                             (-2 * init_speed - discriminant) / (2 * acc.acceleration));
        return res;
    }
    double rest_speed = acc.acceleration * acc.duration + init_speed;
    double acc2_length = (deacc.duration * deacc.duration * deacc.acceleration + 2 * rest_speed * deacc.duration) / 2;
    if (length > acc_length && length <= total_length - acc2_length) {
        double res = acc.duration + (length - acc_length) / rest_speed;
        return res;
    }

    double rest_length = (total_length - acc2_length - acc_length);
    double rest_time = rest_length / rest_speed;

    double discriminant = std::sqrt(4 * rest_speed * rest_speed + 8 * deacc.acceleration * acc2_length);
    double acc2_part_time = std::max((-2 * rest_speed + discriminant) / (2 * deacc.acceleration), (-2 * rest_speed - discriminant) / (2 * deacc.acceleration));
    return acc.duration + rest_time + acc2_part_time;
}

double aed::graph::getStopPenalty() const {
    return stop_penalty;
}

double aed::graph::getDistanceMultiplier() const {
    return distance_multiplier;
}

int aed::graph::getBatches() const {
    return batches;
}

double aed::graph::getBatchDelay() const {
    return batch_delay;
}

double aed::graph::getBatchDistance() const {
    return batch_distance;
}

aed::neighbour::neighbour(const aed::coordinates & coord, double length): coord(coord), length(length), t(), t2(), t3() {}

aed::neighbour::neighbour(const coordinates &coord, double length, const coordinates &continue_dir,
                          const double_vector &t,
                          const double_vector &t2, const double_vector &t3) : coord(coord), length(length),
                                                                                        continue_dir(continue_dir),
                                                                                        t(t), t2(t2), t3(t3) {
}