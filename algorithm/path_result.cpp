#include "path_result.h"

aed::path_result::path_result(double cost, std::vector<aed::path_result_node> && path) : cost(cost), path(std::move(path)) {}

std::ostream & aed::operator<<(std::ostream &os, const path_res_acceleration &acc) {
    os << "duration: " << acc.duration << std::endl;
    os << "acceleration: " << acc.acceleration << std::endl;
    return os;
}

aed::path_result_node::path_result_node(double total_time, double speed, double length,
                                        const path_res_acceleration &start_acc,
                                        const path_res_acceleration &end_acc, const coordinates &vertex,
                                        const double_vector &t1, const double_vector &t2, const double_vector &t3)
        : total_time(total_time), speed(speed), length(length), start_acceleration(start_acc),
          end_acceleration(end_acc), vertex(vertex),
          t1(t1), t2(t2), t3(t3) {}

aed::path_res_acceleration::path_res_acceleration(double duration, double acceleration) : duration(duration), acceleration(acceleration) {}

aed::path_res_acceleration::path_res_acceleration(): duration(0), acceleration(0) {

}

double aed::path_res_acceleration::get_distance(double init_speed) {
    return (duration * duration * acceleration + 2 * init_speed * duration) / 2;
}
