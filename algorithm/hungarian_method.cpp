#include "hungarian_method.h"
#include <algorithm>

std::vector<int> aed::hungarian_method::compute_las(const aed::matrix &orig) {
    aed::matrix cost_reduction(orig.get_n());
    std::vector<double> row_red = initiate_vector(orig.get_n(), std::numeric_limits<double>::infinity());
    std::vector<double> col_red = initiate_vector(orig.get_n(), std::numeric_limits<double>::infinity());
    std::vector<int> row_ass = initiate_vector(orig.get_n(), -1);
    std::vector<int> col_ass = initiate_vector(orig.get_n(), -1);

    preprocessing(orig, cost_reduction, row_red, col_red, row_ass, col_ass);

    std::unordered_set<int> unassigned;
    for (int i = 0; i < orig.get_n(); i++) {
        if (col_ass[i] < 0) {
            unassigned.insert(i);
        }
    }

    std::vector<int> predecessors = initiate_vector(orig.get_n(), -1);

    while (!unassigned.empty()) {
        int row_vertex = *unassigned.begin();

        unassigned.erase(row_vertex);
        int sink = augment(orig.get_n(), row_vertex, orig, row_red, col_red, row_ass, predecessors);

        int row;
        int col = sink;

        do {
            row = predecessors[col];
            row_ass[col] = row;

            int tmp = col_ass[row];
            col_ass[row] = col;
            col = tmp;
        } while (row != row_vertex);
    }

    return row_ass;
}

void aed::hungarian_method::preprocessing(const matrix & orig, matrix & cost_red, std::vector<double> & row_red, std::vector<double> & col_red, std::vector<int> & row_ass, std::vector<int> & col_ass) {
    int n = orig.get_n();

    for (int row = 0; row < n; ++row) {
        row_red[row] = std::numeric_limits<double>::infinity();
        for (int col = 0; col < n; ++col) {
            row_red[row] = std::min(row_red[row], orig.get(row, col));
        }
    }
    for (int col = 0; col < n; ++col) {
        col_red[col] = std::numeric_limits<double>::infinity();

        for (int row = 0; row < n; ++row) {
            col_red[col] = std::min(col_red[col], orig.get(row, col) - row_red[row]);
        }
    }

    for (int row = 0; row < n; ++row) {
        for (int col = 0; col < n; ++col) {
            double reduced_cost = orig.get(row, col) - row_red[row] - col_red[col];
            cost_red.set(row, col, reduced_cost);
            if (row_ass[col] == -1 && equals(reduced_cost, 0)) {
                row_ass[col] = row;
                break;
            }
        }
    }

    for (int col = 0; col < n; ++col) {
        for (int row = 0; row < n; ++row) {
            double reduced_cost = cost_red.get(row, col);

            if (col_ass[row] == -1 && equals(reduced_cost, 0)) {
                col_ass[row] = col;
                break;
            }
        }
    }
}

bool aed::hungarian_method::equals(double a, double b) {
    return std::abs(a - b) < std::numeric_limits<double>::epsilon();
}

template<typename T>
std::vector<T> aed::hungarian_method::initiate_vector(int size, T value) {
    std::vector<T> vector(size);
    for (int i = 0; i < size; ++i) {
        vector[i] = value;
    }

    return vector;
}

int aed::hungarian_method::augment(int n, int row_vertex, const matrix & orig,
                                    std::vector<double> & row_red, std::vector<double> & col_red,
                                    std::vector<int> & row_ass, std::vector<int> & predecessors) {
    std::vector<double> col_min_labeled_cost = initiate_vector(n, std::numeric_limits<double>::infinity());
    std::unordered_set<int> labeled_cols;
    std::unordered_set<int> scanned_rows;
    std::unordered_set<int> scanned_cols;

    std::unordered_set<int> unlabeled_cols;
    for (int i = 0; i < n; ++i) {
        unlabeled_cols.insert(i);
    }

    std::vector<int> tmp;

    std::unordered_set<int> labeled_unscanned_cols;

    int sink = -1;
    int i = row_vertex;

    while (sink < 0) {
        scanned_rows.insert(i);

        for (int col : unlabeled_cols) {
            double reduced_cost = orig.get(i, col) - row_red[i] - col_red[col];

            if (reduced_cost < col_min_labeled_cost[col]) {
                predecessors[col] = i;
                col_min_labeled_cost[col] = reduced_cost;
                if (equals(reduced_cost, 0)) {
                    labeled_cols.insert(col);
                    labeled_unscanned_cols.insert(col);
                    tmp.push_back(col);
                }
            }
        }
        for (int temp: tmp) {
            unlabeled_cols.erase(temp);
        }
        tmp.clear();

        if (labeled_unscanned_cols.empty()) {
            double delta = std::numeric_limits<double>::infinity();

            for (int unlabeled : unlabeled_cols) {
                delta = std::min(delta, col_min_labeled_cost[unlabeled]);
            }

            for (int scanned_row : scanned_rows) {
                row_red[scanned_row] += delta;
            }

            for (int scanned_col : scanned_cols) {
                col_red[scanned_col] -= delta;
            }

            for (int unlabeled : unlabeled_cols) {
                col_min_labeled_cost[unlabeled] -= delta;

                if (equals(col_min_labeled_cost[unlabeled], 0)) {
                    labeled_cols.insert(unlabeled);
                    labeled_unscanned_cols.insert(unlabeled);
                    tmp.push_back(unlabeled);
                }
            }

            for (int temp: tmp) {
                unlabeled_cols.erase(temp);
            }
            tmp.clear();
        }

        if (labeled_unscanned_cols.empty()) {
            throw std::invalid_argument("Internal error. Hungarian method failure.");
        }

        int any_col = *labeled_unscanned_cols.begin();
        scanned_cols.insert(any_col);
        labeled_unscanned_cols.erase(any_col);

        if (row_ass[any_col] < 0) {
            sink = any_col;
        } else {
            i = row_ass[any_col];
        }
    }

    return sink;
}