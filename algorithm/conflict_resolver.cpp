#include "conflict_resolver.h"

void aed::conflict_resolver::add_conflict(const aed::box_occupation &conflict) {
    auto& conflict_map = conflicts[conflict.box];
    conflict_map.insert(std::pair(conflict.time.from, conflict));
}

double
aed::conflict_resolver::conflict_duration(aed::time_interval time, const aed::coordinates &box) const noexcept {
    auto conflict_it = conflicts.find(box);
    if (conflict_it == conflicts.end()) {
        return 0;
    }

    double to_time;
    bool conflict = false;

    auto it = conflict_it->second.lower_bound(time.from);

    if (it != conflict_it->second.begin()) {
        it--;

        if (it->second.time.to > time.from) {
            conflict = true;
            to_time = it->second.time.to;
        }

        it++;
    }

    while (it != conflict_it->second.end()) {
        if (it->first > time.to) {
            break;
        }
        conflict = true;
        to_time = it->second.time.to;
        it++;
    }

    return conflict ? to_time - time.from : 0.0;
}
