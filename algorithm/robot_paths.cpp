#include "robot_paths.h"

#include <utility>

aed::agent::agent(std::unique_ptr<aed::path_result> path, aed::conflict_resolver conflicts) : path(std::move(path)),
                                                                                           conflicts(std::move(conflicts)) {}

aed::robot_paths::robot_paths(double total_cost, std::vector<agent> agents)
        : total_cost(total_cost), agents(std::move(agents)) {

}

aed::agent::agent(const aed::agent &oth) : path(std::make_unique<aed::path_result>(*oth.path)), conflicts(oth.conflicts) {

}

std::ostream& aed::operator<<(std::ostream &os, const aed::robot_paths &robot_paths) {
    os << "Total cost: " << robot_paths.total_cost << std::endl;

    for (int i = 0; i < robot_paths.agents.size(); i++) {
        os << "Agent " << i << ", total total_cost: " << robot_paths.agents[i].path->cost << std::endl;

        for (auto & node : robot_paths.agents[i].path->path) {
            os << "===" << std::endl;
            os << "Vertex: " << node.vertex << std::endl;
            os << "Total time:" << node.total_time << std::endl;
            os << "Speed:" << node.speed << std::endl;
            os << "Length:" << node.length << std::endl;
            os << "Start acceleration:" << std::endl;
            os << node.start_acceleration << std::endl;
            os << "End acceleration:" << std::endl;
            os << node.end_acceleration << std::endl;
            os << "t1: " << node.t1 << std::endl;
            os << "t2: " << node.t2 << std::endl;
            os << "t3: " << node.t3 << std::endl;
        }
    }

    return os;
}
