#ifndef DISPLAY_PATH_RESULT_H
#define DISPLAY_PATH_RESULT_H

#include "coordinates.h"
#include "double_vector.h"
#include <vector>

namespace aed {
    struct path_res_acceleration {
        double duration;
        double acceleration;

        path_res_acceleration();

        path_res_acceleration(double duration, double acceleration);

        double get_distance(double init_speed);
    };

    std::ostream & operator << (std::ostream & os, const path_res_acceleration & acc);

    struct path_result_node {
        double total_time;
        double speed;
        double length;
        path_res_acceleration start_acceleration;
        path_res_acceleration end_acceleration;
        coordinates vertex;
        double_vector t1;
        double_vector t2;
        double_vector t3;

        path_result_node(double total_time, double speed, double length,
                         const path_res_acceleration &start_acc,
                         const path_res_acceleration &end_acc, const coordinates &vertex,
                         const double_vector &t1, const double_vector &t2, const double_vector &t3);
    };

    struct path_result {
        double cost;
        std::vector<path_result_node> path;

        path_result(double cost, std::vector<aed::path_result_node> && path);
    };
}

#endif //DISPLAY_PATH_RESULT_H
