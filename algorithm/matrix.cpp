#include <cstring>
#include "matrix.h"

aed::matrix::matrix(int n) : n(n), data(new double[n * n]) {
    std::memset(data, 0, n*n);
}

double aed::matrix::get(int row, int col) const noexcept {
    return data[row * n + col];
}

void aed::matrix::set(int row, int col, double value) noexcept {
    data[row * n + col] = value;
}

aed::matrix::~matrix() {
    delete [] data;
}

aed::matrix::matrix(const aed::matrix &oth): n(oth.n), data(new double [n * n]) {
    memcpy(data, oth.data, n * n);
}

aed::matrix::matrix(aed::matrix &&oth) noexcept : n(oth.n), data(oth.data) {
    oth.n = 0;
    oth.data = nullptr;
}

int aed::matrix::get_n() const noexcept {
    return n;
}


std::ostream &aed::operator<<(std::ostream &os, const aed::matrix &matrix) {
    for (int row = 0; row < matrix.get_n(); ++row) {
        os << "( ";

        for (int col = 0; col < matrix.get_n(); ++col) {
            if (col > 0) {
                os << ", ";
            }

            os << matrix.get(row, col);
        }

        os << ")" << std::endl;
    }
    return os;
}
