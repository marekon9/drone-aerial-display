#include <cmath>
#include "coordinates.h"

bool aed::coordinates::operator==(const coordinates &oth) const noexcept {
    return x == oth.x && y == oth.y && z == oth.z;
}

aed::coordinates::coordinates(int x, int y, int z) noexcept: x(x), y(y), z(z) {}

double aed::coordinates::distance(const aed::coordinates &oth) const noexcept {
    double x_diff = x - oth.x;
    double y_diff = y - oth.y;
    double z_diff = z - oth.z;
    return std::sqrt(x_diff * x_diff + y_diff * y_diff + z_diff * z_diff);
}

std::ostream &aed::operator<<(std::ostream &os, const aed::coordinates &coord) {
    return os << "[" << coord.x << "," << coord.y << "," << coord.z << "]";
}

bool aed::coordinates::operator!=(const aed::coordinates &oth) const noexcept {
    return !(oth == *this);
}

aed::coordinates::coordinates() : x(0), y(0), z(0) {
}

aed::coordinates aed::coordinates::operator+(const aed::coordinates &oth) const noexcept {
    return aed::coordinates(x + oth.x, y + oth.y, z + oth.z);
}

bool aed::coordinates::is_zero() const noexcept{
    return x == 0 && y == 0 && z == 0;
}

aed::coordinates aed::coordinates::operator-(const aed::coordinates &oth) const noexcept {
    return coordinates(x - oth.x, y - oth.y, z - oth.z);
}

bool aed::coordinates::operator<(const aed::coordinates &oth) const noexcept {
    return z != oth.z ? z < oth.z : y != oth.y ? y < oth.y : x < oth.x;
}

bool aed::coordinates::operator>(const aed::coordinates &oth) const noexcept {
    return z != oth.z ? z > oth.z : y != oth.y ? y > oth.y : x > oth.x;
}

size_t std::hash<aed::coordinates>::operator()(const aed::coordinates &coord) const noexcept {
    size_t res = 13 * 19 + coord.x;
    res *= 19;
    res += coord.y;
    res *= 19;
    res += coord.z;
    return res;
}
