#include <memory>
#include <sstream>
#include <exception>
#include <cmath>
#include <cstring>
#include "parameters.h"
#include "parse_exception.h"

// MACROS

#define IF_STOA(name) if (row.key == #name) {    \
   name = assign_val<std::remove_reference<decltype(name)>::type>(row.value); \
}
#define ELIF_STOA(name) else IF_STOA(name)

#define ELSE_PROP_ERROR else { \
    std::ostringstream oss;    \
    oss << "Unknown variable: " << row.key; \
    throw parse_exception(line, buffer, oss.str()); \
}

#define IF_ELEMENT(name, call) if (row.key == #name) { \
    call;                                                    \
}\


#define ELIF_ELEMENT(name, call) else IF_ELEMENT(name, call)

#define ELSE_ELEMENT_ERROR else { \
    std::ostringstream oss; \
    oss << "Unexpected element: '" << row.key << "'";\
    throw parse_exception(line, buffer, oss.str());                            \
}

#define END_OF_ELEMENT(element) if (row.type != ELEMENT_END || row.key != #element) { \
    throw parse_exception(line, buffer, "Expecting [/"#element"] instead.");             \
}

#define UNEXPECTED_ELEMENT throw parse_exception(line, buffer, "Unexpected element.");

// CODE

aed::parameters::parameters(aed::graph &&graph, int drone_count, double wait_time, std::vector<frame> &&frames, std::vector<std::unique_ptr<aerial_frame_paths>> &&frame_paths) :
        graph(std::move(graph)), drone_count(drone_count), wait_time(wait_time), frames(std::move(frames)), frame_paths(std::move(frame_paths)) {
    validate_parameters();
}

aed::parameters::parameters(const aed::graph &graph, int drone_count, double wait_time, const std::vector<frame> &frames,
                            std::vector<std::unique_ptr<aerial_frame_paths>> && frame_paths) : graph(graph), wait_time(wait_time), drone_count(drone_count), frames(frames), frame_paths(std::move(frame_paths)) {
    validate_parameters();
}

aed::parameters::parameters(std::istream &is) :drone_count(-1), wait_time(0) {
    char buffer[128];
    bool graph_initialized = false;
    int line = 0;

    parsed_row row = parse_row(buffer, line, is);
    while (row.type != READ_EOF) {
        if (row.type == ELEMENT_BEGIN) {
            IF_ELEMENT(graph, graph_initialized = true; parse_graph(graph, buffer, line, is))
            ELIF_ELEMENT(display, parse_display(frames, drone_count, wait_time, buffer, line, is))
            ELIF_ELEMENT(paths, parse_aed_frame_paths(frame_paths, buffer, line, is))
            ELSE_ELEMENT_ERROR
        } else {
            UNEXPECTED_ELEMENT
        }
        row = parse_row(buffer, line, is);
    }

    if (!graph_initialized) {
        throw parse_exception(-1, "", "[graph] element is not present.");
    }

    validate_parameters();
}

void aed::parameters::validate_parameters() {
    validate_graph();
    validate_frames();
    if (!frame_paths.empty()) {
        validate_path();
    }
}

void aed::parameters::validate_graph() {
    std::stringstream ss;
    if (drone_count <= 0) {
        ss << "There should be at least one drone specified, got: " << drone_count;
        throw std::invalid_argument(ss.str());
    }

    if (graph.getLength() <= 0) {
        ss << "Length should be positive, got: " << graph.getLength();
        throw std::invalid_argument(ss.str());
    }
    if (graph.getWidth() <= 0) {
        ss << "Width should be positive, got: " << graph.getWidth();
        throw std::invalid_argument(ss.str());
    }
    if (graph.getHeight() <= 0) {
        ss << "Height should be positive, got: " << graph.getHeight();
        throw std::invalid_argument(ss.str());
    }
    if (graph.getK() < 2) {
        ss << "K has to be at least 2, got: " << graph.getK();
        throw std::invalid_argument(ss.str());
    }

    if (!std::isfinite(graph.getSpace()) || graph.getSpace() <= 0) {
        ss << "Grid size should be final positive number, got: " << graph.getSpace();
        throw std::invalid_argument(ss.str());
    }

    if (graph.getPrecision() <= 0) {
        ss << "Precision should be a positive number, got: " << graph.getPrecision();
        throw std::invalid_argument(ss.str());
    }

    if (!std::isfinite(graph.getCurvature()) || graph.getCurvature() <= 0) {
        ss << "Curvature should be a positive number, got: " << graph.getCurvature();
        throw std::invalid_argument(ss.str());
    }

    double half_space = graph.getSpace() / 2;
    if (graph.getElipsoidA() < 0 || graph.getElipsoidA() > half_space) {
        ss << "Drone length should be between 0 and half of grid size (" << half_space << "), got: " << graph.getElipsoidA();
        throw std::invalid_argument(ss.str());
    }

    if (graph.getElipsoidB() < 0 || graph.getElipsoidB() > half_space) {
        ss << "Drone width should be between 0 and half of grid size (" << half_space << "), got: " << graph.getElipsoidB();
        throw std::invalid_argument(ss.str());
    }

    if (graph.getElipsoidC() < 0 || graph.getElipsoidC() > half_space) {
        ss << "Drone height should be between 0 and half of grid size (" << half_space << "), got: " << graph.getElipsoidC();
        throw std::invalid_argument(ss.str());
    }

    if (!std::isfinite(graph.getMaxSpeed()) || graph.getMaxSpeed() <= 0) {
        ss << "Max speed should be a positive number, got: " << graph.getMaxSpeed();
        throw std::invalid_argument(ss.str());
    }

    double acc = graph.getMaxAcceleration();
    if (!std::isfinite(acc) || acc <= 0) {
        ss << "Maximum acceleration should be a positive number, got: " << acc;
        throw std::invalid_argument(ss.str());
    }

    double time = graph.getMaxSpeed() / acc;

    double distance = (time * time * acc) / 2;

    if (distance > graph.getSpace()) {
        ss << "Max acceleration is too low. It should be possible to stop a drone from max speed within "
           << graph.getSpace() << " distance (equals to the length of grid size). Current breaking distance is "
           << distance << ". Possible fixes: increase acceleration or grid length or decrease max speed.";
        throw std::invalid_argument(ss.str());
    }

}

void aed::parameters::validate_frames() {
    std::ostringstream oss;
    if (frames.size() < 3) {
        oss << "There should be at least 3 frames, got: " << frames.size();
        throw std::invalid_argument(oss.str());
    }

    std::unordered_set<coordinates> duplicates;
    std::vector<int> pos(drone_count);

    for (size_t i = 0; i < frames.size(); i++) {
        duplicates.clear();
        std::memset((void *) pos.data(), 0, drone_count * sizeof(int));
        const auto & frame = frames[i];

        if (frame.positions.size() != drone_count) {
            oss << "Frame num. " << i << " has invalid number of positions. Expected: " << drone_count << ", got: " << frame.positions.size();
        }

        for (const auto & position : frame.positions) {
            bool inserted = duplicates.insert(position.position).second;
            if (!inserted) {
                oss << "Detected duplicate position " << position.position << " in frame num. " << i;
                throw std::invalid_argument(oss.str());
            }

            if (position.agent < -1 || position.agent >= drone_count) {
                oss << "Agent number " << position.agent << " is out of range [ -1, " << (drone_count - 1) << "] for position at coordinates " << position.position << " in frame num. " << i;
                throw std::invalid_argument(oss.str());
            }

            if (position.agent >= 0) {
                pos[position.agent]++;

                if (pos[position.agent] > 1) {
                    oss << "Agent number " << position.agent << " cannot be at multiple places in frame num. " << i;
                    throw std::invalid_argument(oss.str());
                }
            }

            if (position.position.x < 0 || position.position.x >= graph.getLength() ||
                position.position.y < 0 || position.position.y >= graph.getWidth() ||
                position.position.z < 0 || position.position.z >= graph.getHeight()) {
                oss << "Detected position " << position.position << " which lies outside of the display in frame num. " << i;
                throw std::invalid_argument(oss.str());
            }
        }
    }

    const char* names[] = {"first", "last"};
    size_t indexes[] = {0, frames.size() - 1};

    for (size_t i : indexes) {
        for (const auto & position : frames[i].positions) {
            if (position.position.z > 0) {
                oss << "Detected position " << position.position << " in the " << names[i] << " frame. Positions in the " << names[i] << " should have z coordinate equal to 0.";
                throw std::invalid_argument(oss.str());
            }
        }
    }
}

void aed::parameters::validate_path() {
    std::ostringstream oss;

    if (frame_paths.size() != frames.size() - 1) {
        oss << "There should be " << (frames.size() - 1) << " path result frames, got " << frame_paths.size() << " instead.";
        throw std::invalid_argument(oss.str());
    }

    for (size_t i = 0; i < frame_paths.size(); i++) {
        const auto & path_res = frame_paths[i];
        if (path_res == nullptr) {
            continue;
        }
        for (const auto & drone : path_res->drones) {
            if (drone.path != nullptr) {
                for (const auto & node : drone.path->path) {
                    if (node.vertex.x < 0 || node.vertex.x >= graph.getLength() ||
                        node.vertex.y < 0 || node.vertex.y >= graph.getWidth() ||
                        node.vertex.z < 0 || node.vertex.z >= graph.getHeight()) {
                        oss << "Path frame " << i << " contains a drone with invalid vertex position: " << node.vertex;
                        throw std::invalid_argument(oss.str());
                    }
                }
            }
        }
    }

}

aed::parameters::parsed_row aed::parameters::parse_row(char * buffer, int & line, std::istream &is) {
    char c;

    do {
        is.getline(buffer, 128);
        if (is.eof()) {
            return parsed_row{READ_EOF};
        }

        if (is.bad()) {
            return parsed_row{READ_FAILURE};
        }

        c = buffer[0];
        line++;
    } while (c == '#' || c == '\n' || c == '\r' || c == '\0');

    int i = 0;
    bool element = c == '[';
    bool element_end = false;
    if (element) {
        i++;

        if (buffer[1] == '/') {
            element_end = true;
            i++;
        }
    }

    char key_buf[128];
    int key_i = 0;

    char val_buf[128];
    int val_i = 0;

    char* current_buff = key_buf;
    int* curr_i = &key_i;

    while (i < 128 && c != '\0' && c != '\n' && c != '\r') {
        c = buffer[i++];

        if (c == '=' && !element) {
            current_buff[*curr_i] = '\0';
            current_buff = val_buf;
            curr_i = &val_i;
        } else if (c == ']' && element) {
            current_buff[*curr_i] ='\0';
            return parsed_row{element_end ? ELEMENT_END : ELEMENT_BEGIN, key_buf, ""};
        } else {
            current_buff[*curr_i] = c;
            (*curr_i)++;
        }
    }

    if (element || val_i == 0) {
        return parsed_row{READ_FAILURE};
    }

    val_buf[val_i] = '\0';

    return parsed_row{PAIR, key_buf, val_buf};
}

namespace aed {
    template <typename T> T assign_val(const std::string & str);

    template <> int assign_val(const std::string & str) {
        return std::stoi(str);
    }

    template <> double assign_val(const std::string & str) {
        return std::stod(str);
    }
}

void aed::parameters::parse_graph(aed::graph &graph, char *buffer, int &line, std::istream &is) {
    int length = 0;
    int width = 0;
    int height = 0;
    int k = 0;
    int precision = 0;
    double curvature = 0;
    double grid_size = 0;
    double drone_length = 0;
    double drone_width = 0;
    double drone_height = 0;
    double max_speed = 0;
    double max_acceleration;
    double stop_penalty = -1;
    double distance_multiplier = 0;
    int batches = 0;
    double batch_delay = 0;
    double batch_distance = 0;

    parsed_row row = parse_row(buffer, line, is);
    while (row.type == PAIR) {
        IF_STOA(length)
        ELIF_STOA(width)
        ELIF_STOA(height)
        ELIF_STOA(k)
        ELIF_STOA(precision)
        ELIF_STOA(curvature)
        ELIF_STOA(grid_size)
        ELIF_STOA(drone_length)
        ELIF_STOA(drone_width)
        ELIF_STOA(drone_height)
        ELIF_STOA(max_speed)
        ELIF_STOA(max_acceleration)
        ELIF_STOA(stop_penalty)
        ELIF_STOA(distance_multiplier)
        ELIF_STOA(batches)
        ELIF_STOA(batch_delay)
        ELIF_STOA(batch_distance)
        ELSE_PROP_ERROR

        row = parse_row(buffer, line, is);
    }

    END_OF_ELEMENT(graph)

    std::vector<std::string> invalid_props;
    validate_property(invalid_props, "length", length);
    validate_property(invalid_props, "width", width);
    validate_property(invalid_props, "height", height);
    validate_property(invalid_props, "k", k - 1);
    validate_property(invalid_props, "precision", precision);
    validate_property(invalid_props, "curvature", curvature);
    validate_property(invalid_props, "grid_size", grid_size);
    validate_property(invalid_props, "drone_length", drone_length);
    validate_property(invalid_props, "drone_width", drone_width);
    validate_property(invalid_props, "drone_height", drone_height);
    validate_property(invalid_props, "max_speed", max_speed);
    validate_property(invalid_props, "max_acceleration", max_acceleration);
    validate_non_negative_property(invalid_props, "stop_penalty", stop_penalty);
    validate_non_negative_property(invalid_props, "distance_multiplier", distance_multiplier);
    validate_non_negative_property(invalid_props, "batch_delay", batch_delay);
    validate_non_negative_property(invalid_props, "batch_distance", batch_distance);


    validate_props(line, buffer, invalid_props);

    graph = aed::graph(length, width, height, k, precision, curvature, grid_size, drone_length, drone_width,
                       drone_height, max_speed, max_acceleration, stop_penalty, distance_multiplier, batches,
                       batch_delay, batch_distance);
}

template<typename T>
void aed::parameters::validate_property(std::vector<std::string> &props, const std::string &name, T value) {
    if (value <= 0) {
        props.push_back(name);
    }
}

template<typename T>
void aed::parameters::validate_non_negative_property(std::vector<std::string> &props, const std::string &name, T value) {
    if (value < 0) {
        props.push_back(name);
    }
}

void
aed::parameters::validate_bool_property(std::vector<std::string> &props, const std::string &name, bool initialized) {
    if (!initialized) {
        props.push_back(name);
    }
}

void aed::parameters::validate_props(int line, char * buffer, std::vector<std::string> &props) {
    if (!props.empty()) {
        std::ostringstream oss;
        oss << "Found uninitialized variables: [";

        bool first = true;
        for (const auto & prop : props) {
            if (first) {
                first = false;
            } else {
                oss << ", ";
            }
            oss << prop;
        }

        oss << "]";

        throw parse_exception(line, buffer, oss.str());
    }
}

void aed::parameters::parse_display(std::vector<frame> &frames, int &drone_count, double & wait_time, char *buffer, int &line,
                                    std::istream &is) {
    parsed_row row = parse_row(buffer, line, is);

    while (row.type != ELEMENT_END) {
        switch (row.type) {
            case PAIR:
                IF_STOA(drone_count)
                ELIF_STOA(wait_time)
                ELSE_PROP_ERROR
                break;
            case ELEMENT_BEGIN:
                IF_ELEMENT(frames, parse_frames(frames, buffer, line, is))
                ELSE_ELEMENT_ERROR
                break;
            default:
                throw parse_exception(line, buffer, "Unexpected element.");
        }
        row = parse_row(buffer, line, is);
    }

    END_OF_ELEMENT(display)

}

void aed::parameters::parse_frames(std::vector<frame> &frames, char *buffer, int &line, std::istream &is) {
    parsed_row row = parse_row(buffer, line, is);

    while (row.type != ELEMENT_END) {
        if (row.type == ELEMENT_BEGIN) {
            IF_ELEMENT(frame, parse_frame(frames, buffer, line, is))
            ELSE_ELEMENT_ERROR
        } else {
            UNEXPECTED_ELEMENT
        }

        row = parse_row(buffer, line, is);
    }

    END_OF_ELEMENT(frames)
}

void aed::parameters::parse_frame(std::vector<frame> &frames, char *buffer, int &line, std::istream &is) {
    parsed_row row = parse_row(buffer, line, is);
    std::vector<drone_position> positions;

    while (row.type != ELEMENT_END) {
        if (row.type == ELEMENT_BEGIN) {
            IF_ELEMENT(position, parse_drone_positions(positions, buffer, line, is))
            ELSE_ELEMENT_ERROR
        } else {
            UNEXPECTED_ELEMENT
        }
        row = parse_row(buffer, line, is);
    }

    END_OF_ELEMENT(frame)

    frames.emplace_back(std::move(positions));
}

void aed::parameters::parse_drone_positions(std::vector<drone_position> &position, char *buffer, int &line,
                                            std::istream &is) {
    int agent = -1;
    bool color_initialized = false;
    color color;
    bool coordinates_initialized = false;
    coordinates coordinates;

    parsed_row row = parse_row(buffer, line, is);

    while (row.type != ELEMENT_END) {
        if (row.type == PAIR) {
            IF_STOA(agent)
            ELSE_PROP_ERROR
        } else if (row.type == ELEMENT_BEGIN) {
            IF_ELEMENT(color, color_initialized = true; parse_color(color, buffer, line, is))
            ELIF_ELEMENT(coordinates, coordinates_initialized = true; parse_coordinates(coordinates, buffer, line, is))
            ELSE_ELEMENT_ERROR
        } else {
            UNEXPECTED_ELEMENT
        }

        row = parse_row(buffer, line, is);
    }

    END_OF_ELEMENT(position)

    std::vector<std::string> invalid_props;
    validate_bool_property(invalid_props, "color", color_initialized);
    validate_bool_property(invalid_props, "coordinates", coordinates_initialized);
    validate_props(line, buffer, invalid_props);

    position.emplace_back(coordinates, color, agent);
}

void aed::parameters::parse_color(aed::color &color, char *buffer, int &line, std::istream &is, const std::string& element) {
    int red = -1;
    int green = -1;
    int blue = -1;
    int active = -1;

    parsed_row row = parse_row(buffer, line, is);
    while (row.type != ELEMENT_END) {
        if (row.type == PAIR) {
            IF_STOA(red)
            ELIF_STOA(green)
            ELIF_STOA(blue)
            ELIF_STOA(active)
            ELSE_PROP_ERROR
        } else {
            UNEXPECTED_ELEMENT
        }
        row = parse_row(buffer, line, is);
    }

    if (row.type != ELEMENT_END || row.key != element) {
        std::ostringstream oss;
        oss << "Expecting [" << element << "] instead.";
        throw std::invalid_argument(oss.str());

    }

    std::vector<std::string> invalid_params;
    validate_property(invalid_params, "red", red + 1);
    validate_property(invalid_params, "green", green + 1);
    validate_property(invalid_params, "blue", blue + 1);
    validate_property(invalid_params, "active", active + 1);

    color.red = red;
    color.green = green;
    color.blue = blue;
    color.active = active > 0;
}

void aed::parameters::parse_coordinates(aed::coordinates &coordinates, char *buffer, int &line, std::istream &is, const std::string & element) {
    int x = -1;
    int y = -1;
    int z = -1;

    parsed_row row = parse_row(buffer, line, is);

    while (row.type != ELEMENT_END) {
        if (row.type == PAIR) {
            IF_STOA(x)
            ELIF_STOA(y)
            ELIF_STOA(z)
            ELSE_PROP_ERROR
        } else {
            UNEXPECTED_ELEMENT
        }

        row = parse_row(buffer, line, is);
    }

    if (row.type != ELEMENT_END || row.key != element) {
        std::ostringstream oss;
        oss << "Expecting [" << element << "] instead.";
        throw std::invalid_argument(oss.str());
    }

    std::vector<std::string> invalid_params;
    validate_property(invalid_params, "x", x + 1);
    validate_property(invalid_params, "y", y + 1);
    validate_property(invalid_params, "z", z + 1);

    validate_props(line, buffer, invalid_params);

    coordinates.x = x;
    coordinates.y = y;
    coordinates.z = z;
}

void aed::parameters::parse_aed_frame_paths(std::vector<std::unique_ptr<aerial_frame_paths>> &frame_paths, char *buffer, int &line,
                                            std::istream &is) {
    parsed_row row = parse_row(buffer, line, is);

    while (row.type != ELEMENT_END) {
        if (row.type == ELEMENT_BEGIN) {
            IF_ELEMENT(frame, parse_aed_frame(frame_paths, buffer, line, is))
            ELIF_ELEMENT(frame_missing, parse_aed_frame_missing(frame_paths, buffer, line, is))
            ELSE_ELEMENT_ERROR
        } else {
            UNEXPECTED_ELEMENT
        }
        row = parse_row(buffer, line, is);
    }

    END_OF_ELEMENT(paths)
}

void
aed::parameters::parse_aed_frame_missing(std::vector<std::unique_ptr<aerial_frame_paths>> &frame_paths, char *buffer,
                                         int &line, std::istream &is) {
    frame_paths.push_back(nullptr);

    parsed_row row = parse_row(buffer, line, is);

    END_OF_ELEMENT(frame_missing)

}

void aed::parameters::parse_aed_frame(std::vector<std::unique_ptr<aerial_frame_paths>> &frame_paths, char *buffer, int &line,
                                      std::istream &is) {
    double total_cost = 0;
    double max_cost = 0;
    std::vector<aerial_frame_drone> drones;

    parsed_row row = parse_row(buffer, line, is);
    while (row.type != ELEMENT_END) {
        if (row.type == PAIR) {
            IF_STOA(total_cost)
            ELIF_STOA(max_cost)
            ELSE_PROP_ERROR
        } else if (row.type == ELEMENT_BEGIN) {
            IF_ELEMENT(drones, parse_aed_frame_drones(drones, buffer, line, is))
            ELSE_ELEMENT_ERROR
        } else {
            UNEXPECTED_ELEMENT
        }
        row = parse_row(buffer, line, is);
    }

    END_OF_ELEMENT(frame)

    std::vector<std::string> invalid_params;
    validate_property(invalid_params, "total_cost", total_cost);
    validate_property(invalid_params, "max_cost", total_cost);
    validate_bool_property(invalid_params, "drones", !drones.empty());

    validate_props(line, buffer, invalid_params);

    frame_paths.push_back(std::make_unique<aerial_frame_paths>(aerial_frame_paths{total_cost, max_cost, std::move(drones)}));
}

void aed::parameters::parse_aed_frame_drones(std::vector<aerial_frame_drone> &frame_drone, char *buffer, int &line,
                                             std::istream &is) {
    parsed_row row = parse_row(buffer, line, is);

    while (row.type != ELEMENT_END) {
        if (row.type == ELEMENT_BEGIN) {
            IF_ELEMENT(drone, parse_aed_frame_drone(frame_drone, buffer, line, is))
            ELSE_ELEMENT_ERROR
        } else {
            UNEXPECTED_ELEMENT
        }
        row = parse_row(buffer, line, is);
    }

    END_OF_ELEMENT(drones)
}

void aed::parameters::parse_aed_frame_drone(std::vector<aerial_frame_drone> &frame_drone, char *buffer, int &line,
                                            std::istream &is) {
    std::unique_ptr<path_result> path = nullptr;

    bool from_color_initialized = false;
    color from_color;

    bool to_color_initialized = false;
    color to_color;

    parsed_row row = parse_row(buffer, line, is);

    while (row.type != ELEMENT_END) {
        if (row.type == ELEMENT_BEGIN) {
            IF_ELEMENT(from_color, from_color_initialized = true; parse_color(from_color, buffer, line, is, "from_color"))
            ELIF_ELEMENT(to_color, to_color_initialized = true; parse_color(to_color, buffer, line, is, "to_color"))
            ELIF_ELEMENT(path, path = parse_path(buffer, line, is))
            ELSE_ELEMENT_ERROR
        } else {
            UNEXPECTED_ELEMENT
        }

        row = parse_row(buffer, line, is);
    }

    END_OF_ELEMENT(drone)

    std::vector<std::string> invalid_params;
    validate_bool_property(invalid_params, "from_color", from_color_initialized);
    validate_bool_property(invalid_params, "to_color", to_color_initialized);

    validate_props(line, buffer, invalid_params);

    frame_drone.push_back(aerial_frame_drone{std::move(path), from_color, to_color});
}

std::unique_ptr<aed::path_result> aed::parameters::parse_path(char *buffer, int &line, std::istream &is) {
    double cost = -1;
    std::vector<path_result_node> nodes;

    parsed_row row = parse_row(buffer, line, is);
    while (row.type != ELEMENT_END) {
        if (row.type == PAIR) {
            IF_STOA(cost)
            ELSE_PROP_ERROR
        } else if (row.type == ELEMENT_BEGIN) {
            IF_ELEMENT(path, parse_path_frame_result(nodes, buffer, line, is))
            ELSE_ELEMENT_ERROR
        } else {
            UNEXPECTED_ELEMENT
        }

        row = parse_row(buffer, line, is);

    }

    END_OF_ELEMENT(path)

    std::vector<std::string> invalid_params;
    validate_property(invalid_params, "cost", cost);

    return std::make_unique<path_result>(cost, std::move(nodes));
}

void aed::parameters::parse_path_frame_result(std::vector<path_result_node> &path, char *buffer, int &line,
                                              std::istream &is) {
    parsed_row row = parse_row(buffer, line, is);

    while (row.type != ELEMENT_END) {
        if (row.type == ELEMENT_BEGIN) {
            IF_ELEMENT(node, parse_path_frame_result_node(path, buffer, line, is))
            ELSE_ELEMENT_ERROR
        } else {
            UNEXPECTED_ELEMENT
        }
        row = parse_row(buffer, line, is);
    }

    END_OF_ELEMENT(path)
}

void aed::parameters::parse_path_frame_result_node(std::vector<path_result_node> &path, char *buffer, int &line,
                                                   std::istream &is) {
    double cost = -1;
    double speed = -1;
    double length = -1;
    double start_acc = 0;
    double start_acc_duration = 0;
    double end_acc = 0;
    double end_acc_duration = 0;
    bool coordinates_initialized = false;
    bool next_dir_initialized = false;
    coordinates coordinates;

    double_vector t1;
    double_vector t2;
    double_vector t3;

    parsed_row row = parse_row(buffer, line, is);

    while (row.type != ELEMENT_END) {
        if (row.type == ELEMENT_BEGIN) {
            IF_ELEMENT(coordinates, coordinates_initialized = true; parse_coordinates(coordinates, buffer, line, is))
            ELIF_ELEMENT(t1, parse_double_vector(t1, buffer, line, is, "t1"))
            ELIF_ELEMENT(t2, parse_double_vector(t2, buffer, line, is, "t2"))
            ELIF_ELEMENT(t3, parse_double_vector(t3, buffer, line, is, "t3"))
            ELSE_ELEMENT_ERROR
        } else if (row.type == PAIR) {
            IF_STOA(cost)
            ELIF_STOA(speed)
            ELIF_STOA(length)
            ELIF_STOA(start_acc)
            ELIF_STOA(start_acc_duration)
            ELIF_STOA(end_acc)
            ELIF_STOA(end_acc_duration)
            ELSE_PROP_ERROR
        } else {
            UNEXPECTED_ELEMENT
        }
        row = parse_row(buffer, line, is);
    }

    END_OF_ELEMENT(node)

    std::vector<std::string> invalid_params;
    validate_property(invalid_params, "cost", cost);
    validate_non_negative_property(invalid_params, "speed", speed);
    validate_non_negative_property(invalid_params, "length", length);
    validate_non_negative_property(invalid_params, "start_acc_duration", start_acc_duration);
    validate_non_negative_property(invalid_params, "end_acc_duration", end_acc_duration);
    validate_bool_property(invalid_params, "coordinates", coordinates_initialized);

    path.emplace_back(cost, speed, length, path_res_acceleration(start_acc_duration, start_acc),
                                    path_res_acceleration(end_acc_duration, end_acc), coordinates, t1, t2, t3);
}

void aed::parameters::parse_double_vector(aed::double_vector &vector, char *buffer, int &line, std::istream &is,
                                          const std::string &element) {
    double x = 0;
    double y = 0;
    double z = 0;

    parsed_row row = parse_row(buffer, line, is);

    while (row.type != ELEMENT_END) {
        if (row.type == PAIR) {
            IF_STOA(x)
            ELIF_STOA(y)
            ELIF_STOA(z)
            ELSE_PROP_ERROR
        } else {
            UNEXPECTED_ELEMENT
        }

        row = parse_row(buffer, line, is);
    }

    if (row.type != ELEMENT_END || row.key != element) {
        std::ostringstream oss;
        oss << "Expecting [" << element << "] instead.";
        throw std::invalid_argument(oss.str());
    }

    vector.x = x;
    vector.y = y;
    vector.z = z;
}

void aed::parameters::save(std::ostream &os, bool include_results) const {
    os << "[graph]" << std::endl;
    os << "length=" << graph.getLength() << std::endl;
    os << "width=" << graph.getWidth() << std::endl;
    os << "height=" << graph.getHeight() << std::endl;
    os << "k=" << graph.getK() << std::endl;
    os << "precision=" << graph.getPrecision() << std::endl;
    os << "curvature=" << graph.getCurvature() << std::endl;
    os << "grid_size=" << graph.getSpace() << std::endl;
    os << "drone_length=" << graph.getElipsoidA() << std::endl;
    os << "drone_width=" << graph.getElipsoidB() << std::endl;
    os << "drone_height=" << graph.getElipsoidC() << std::endl;
    os << "max_speed=" << graph.getMaxSpeed() << std::endl;
    os << "max_acceleration=" << graph.getMaxAcceleration() << std::endl;
    os << "stop_penalty=" << graph.getStopPenalty() << std::endl;
    os << "distance_multiplier=" << graph.getDistanceMultiplier() << std::endl;
    os << "batches=" << graph.getBatches() << std::endl;
    os << "batch_delay=" << graph.getBatchDelay() << std::endl;
    os << "batch_distance=" << graph.getBatchDistance() << std::endl;
    os << "[/graph]" << std::endl;
    os << std::endl << std::endl;

    os << "[display]" << std::endl;
    os << "drone_count=" << drone_count << std::endl;
    os << "wait_time=" << wait_time << std::endl;
    os << "[frames]" << std::endl;

    for (const auto & frame : frames) {
        os << "[frame]" << std::endl;
        for (const auto & position : frame.positions) {
            os << "[position]" << std::endl;
            os << "agent=" << position.agent << std::endl;
            os << "[coordinates]" << std::endl;
            os << "x=" << position.position.x << std::endl;
            os << "y=" << position.position.y << std::endl;
            os << "z=" << position.position.z << std::endl;
            os << "[/coordinates]" << std::endl;
            os << "[color]" << std::endl;
            os << "red=" << (int) position.color.red << std::endl;
            os << "green=" << (int) position.color.green << std::endl;
            os << "blue=" << (int) position.color.blue << std::endl;
            os << "active=" << position.color.active << std::endl;
            os << "[/color]" << std::endl;
            os << "[/position]" << std::endl;
        }

        os << "[/frame]" << std::endl;
    }

    os << "[/frames]" << std::endl;
    os << "[/display]" << std::endl;

    if (include_results) {
        os << "[paths]" << std::endl;

        for (const auto & frame_path : frame_paths) {
            if (frame_path == nullptr) {
                os << "[frame_missing]" << std::endl;
                os << "[/frame_missing]" << std::endl;
                continue;
            }
            os << "[frame]" << std::endl;
            os << "total_cost=" << frame_path->total_cost << std::endl;
            os << "max_cost=" << frame_path->max_cost << std::endl;
            os << "[drones]" << std::endl;

            for (const auto & drone : frame_path->drones) {
                os << "[drone]" << std::endl;

                if (drone.path != nullptr) {
                    os << "[path]" << std::endl;

                    os << "cost=" << drone.path->cost << std::endl;

                    os << "[path]" << std::endl;

                    for (const auto & element : drone.path->path) {
                        os << "[node]" << std::endl;
                        os << "cost=" << element.total_time << std::endl;
                        os << "length=" << element.length << std::endl;
                        os << "speed=" << element.speed << std::endl;
                        os << "start_acc=" << element.start_acceleration.acceleration << std::endl;
                        os << "start_acc_duration=" << element.start_acceleration.duration << std::endl;
                        os << "end_acc=" << element.end_acceleration.acceleration << std::endl;
                        os << "end_acc_duration=" << element.end_acceleration.duration << std::endl;
                        os << "[coordinates]" << std::endl;
                        os << "x=" << element.vertex.x << std::endl;
                        os << "y=" << element.vertex.y << std::endl;
                        os << "z=" << element.vertex.z << std::endl;
                        os << "[/coordinates]" << std::endl;
                        os << "[t1]" << std::endl;
                        os << "x=" << element.t1.x << std::endl;
                        os << "y=" << element.t1.y << std::endl;
                        os << "z=" << element.t1.z << std::endl;
                        os << "[/t1]" << std::endl;
                        os << "[t2]" << std::endl;
                        os << "x=" << element.t2.x << std::endl;
                        os << "y=" << element.t2.y << std::endl;
                        os << "z=" << element.t2.z << std::endl;
                        os << "[/t2]" << std::endl;
                        os << "[t3]" << std::endl;
                        os << "x=" << element.t3.x << std::endl;
                        os << "y=" << element.t3.y << std::endl;
                        os << "z=" << element.t3.z << std::endl;
                        os << "[/t3]" << std::endl;
                        os << "[/node]" << std::endl;
                    }
                    os << "[/path]" << std::endl;
                    os << "[/path]" << std::endl;
                }

                os << "[from_color]" << std::endl;
                os << "red=" << (int) drone.from_color.red << std::endl;
                os << "green=" << (int) drone.from_color.green << std::endl;
                os << "blue=" << (int) drone.from_color.blue << std::endl;
                os << "active=" << drone.from_color.active << std::endl;
                os << "[/from_color]" << std::endl;

                os << "[to_color]" << std::endl;
                os << "red=" << (int) drone.to_color.red << std::endl;
                os << "green=" << (int) drone.to_color.green << std::endl;
                os << "blue=" << (int) drone.to_color.blue << std::endl;
                os << "active=" << drone.to_color.active << std::endl;
                os << "[/to_color]" << std::endl;

                os << "[/drone]" << std::endl;
            }


            os << "[/drones]" << std::endl;
            os << "[/frame]" << std::endl;
        }

        os << "[/paths]" << std::endl;
    }
}

const aed::graph &aed::parameters::get_graph() const noexcept {
    return graph;
}

const std::vector<aed::frame> &aed::parameters::get_frames() const noexcept {
    return frames;
}

const std::vector<std::unique_ptr<aed::aerial_frame_paths>> &aed::parameters::get_frame_paths() const noexcept {
    return frame_paths;
}

int aed::parameters::get_drone_count() const noexcept {
    return drone_count;
}

double aed::parameters::get_wait_time() const noexcept {
    return wait_time;
}

void aed::parameters::set_frame_paths(std::vector<std::unique_ptr<aerial_frame_paths>> &&paths) {
    this->frame_paths = std::move(paths);
}


