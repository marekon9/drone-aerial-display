#include "double_vector.h"
#include <cmath>

aed::double_vector::double_vector(double x, double y, double z) : x(x), y(y), z(z) {}

aed::double_vector::double_vector(): x(0), y(0), z(0) {
}

double aed::double_vector::distance(const aed::double_vector &oth) const {
    double a = x - oth.x;
    double b = y - oth.y;
    double c = z - oth.z;
    return std::sqrt(a*a + b*b + c*c);
}

aed::double_vector aed::double_vector::operator+(const aed::double_vector & oth) const {
    return aed::double_vector(x + oth.x, y + oth.y, z + oth.z);
}

std::ostream & aed::operator<<(std::ostream &os, const double_vector &vector) {
    os << "[" << vector.x << ", " << vector.y << ", " << vector.z << "]";
    return os;
}
