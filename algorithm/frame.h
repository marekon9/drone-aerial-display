#ifndef DISPLAY_FRAME_H
#define DISPLAY_FRAME_H

#include <vector>
#include "drone_position.h"

namespace aed {
    struct frame {
        std::vector<drone_position> positions;

        explicit frame(const std::vector<drone_position> & positions);

        explicit frame(std::vector<drone_position> && positions);
    };
}


#endif //DISPLAY_FRAME_H
