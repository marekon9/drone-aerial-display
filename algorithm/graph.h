#ifndef DISPLAY_GRAPH_H
#define DISPLAY_GRAPH_H

#include <vector>
#include <unordered_set>
#include <unordered_map>
#include <set>
#include "coordinates.h"
#include "box_occupation.h"
#include "double_vector.h"
#include "path_result.h"

namespace aed {
    struct neighbour {
        coordinates coord;
        double length;

        coordinates continue_dir;
        double_vector t;
        double_vector t2;
        double_vector t3;

        neighbour(const coordinates & coord, double length);

        neighbour(const coordinates &coord, double length, const coordinates &continue_dir, const double_vector &t,
                  const double_vector &t2, const double_vector &t3);
    };

    struct axis_occupation {
        time_interval time;
        double from_length;
        int axis_coordinate;
    };

    class graph {
        int length;
        int width;
        int height;
        int k;
        int precision;
        double curvature;
        double space;
        double elipsoid_a;
        double elipsoid_b;
        double elipsoid_c;
        double max_speed;
        double max_acceleration;
        double stop_penalty;
        double distance_multiplier;
        int batches;
        double batch_delay;
        double batch_distance;

    private:

        // cache
        std::vector<neighbour> basic_case;
        std::unordered_map<coordinates, std::vector<neighbour>> neighborhood;

        // reusable vectors
        std::vector<axis_occupation> x_intervals;
        std::vector<axis_occupation> y_intervals;
        std::vector<axis_occupation> z_intervals;
        std::vector<std::pair<double, int>> rights_x;
        std::vector<std::pair<double, int>> lefts_x;
        std::vector<std::pair<double, int>> rights_y;
        std::vector<std::pair<double, int>> lefts_y;
        std::vector<std::pair<double, int>> rights_z;
        std::vector<std::pair<double, int>> lefts_z;

        // static helpers
        static double
        length_to_time(double length, double total_length, double init_speed, const path_res_acceleration &acc,
                       const path_res_acceleration &deacc);

        static double compute_cos(double coord_dist, int b, int c);

        static int sgn(int a) noexcept;

        void compute_neighborhood_in_direction(const coordinates &direction, std::vector<neighbour> &neighbors);

        static std::vector<aed::neighbour> compute_neighbourhood(int k, double space);

        void all_init_neighbors();

        static std::vector<neighbour> get_init_neighbors(int k, double space);

        static bool is_duplicate_neighbour(const coordinates &coord, int m_x, int m_y, int m_z) ;

        static void max_time_and_length(const axis_occupation &x_interval, const axis_occupation &y_interval,
                                        const axis_occupation &z_interval, double &from_time, double &from_length) ;


        void compute_current_quadrant(double path_length, double ratio, double drone_size, double t1, double t2, double t3, double & left, int & left_quadrant, int prev_left_quadrant, std::vector<std::pair<double, int>>& lefts, double & right, int & right_quadrant, int prev_right_quadrant, std::vector<std::pair<double, int>>& rights);

        std::vector<aed::axis_occupation> &
        make_axis_intervals(const aed::neighbour &neighbor, double init_speed, const path_res_acceleration &acc,
                            const path_res_acceleration &deacc, std::vector<axis_occupation> &intervals,
                            const std::vector<std::pair<double, int>> &rights_x,
                            const std::vector<std::pair<double, int>> &lefts_x) const;

        void linear_intervals(const aed::neighbour &neighbor, double init_speed, double time,
                              const path_res_acceleration &acc, const path_res_acceleration &deacc);

        void linear_durations(std::vector<axis_occupation> &intervals, double max_distance, double drone_size, int dist,
                              double cos, double time, double init_speed, const path_res_acceleration &acc,
                              const path_res_acceleration &deacc);

        void curved_intervals(const aed::neighbour &neighbor, double init_speed, double time,
                              const path_res_acceleration &acc, const path_res_acceleration &deacc);

        void clear_vectors(std::vector<box_occupation> &res);

    public:

        graph() = default;

        graph(int length, int width, int height, int k, int precision, double curvature, double space,
              double elipsoid_a,
              double elipsoid_b, double elipsoid_c, double max_speed, double max_acceleration, double stop_penalty,
              double distance_multiplier,
              int batches,
              double batch_delay,
              double batch_distance);

        const std::vector<neighbour>& get_neighborhood_in_direction(const coordinates & direction);

        const std::vector<neighbour>& get_stop_neighborhood_in_direction(std::vector<neighbour>& neighbors, const coordinates & direction);

        void find_trajectory(std::vector<box_occupation> &res, const aed::neighbour &neighbor, double init_speed,
                             double time, const path_res_acceleration &acc, const path_res_acceleration &deacc);

        bool is_in_graph(const coordinates & pos) const;

        int getLength() const;

        int getWidth() const;

        int getHeight() const;

        int getK() const;

        double getSpace() const;

        double getElipsoidA() const;

        double getElipsoidB() const;

        double getElipsoidC() const;

        int getPrecision() const;

        double getCurvature() const;

        double getMaxSpeed() const;

        double getMaxAcceleration() const;

        double getStopPenalty() const;

        double getDistanceMultiplier() const;

        int getBatches() const;

        double getBatchDelay() const;

        double getBatchDistance() const;

    };
}

#endif //DISPLAY_GRAPH_H
