#include "parse_exception.h"

aed::parse_exception::parse_exception(int line, const std::string &content, const std::string &reason)
        : line(line), content(content), reason(reason) {}

int aed::parse_exception::get_line() const {
    return line;
}

const std::string &aed::parse_exception::get_content() const {
    return content;
}

const std::string &aed::parse_exception::get_reason() const {
    return reason;
}

const char *aed::parse_exception::what() const noexcept {
    return reason.c_str();
}
