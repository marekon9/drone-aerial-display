#ifndef DISPLAY_DOUBLE_VECTOR_H
#define DISPLAY_DOUBLE_VECTOR_H

#include <iostream>

namespace aed {
    struct double_vector {
        double x;
        double y;
        double z;

        double_vector();

        double_vector(double x, double y, double z);

        double distance(const double_vector & oth) const;

        double_vector operator+ (const double_vector & oth) const;
    };

    std::ostream & operator << (std::ostream & os, const double_vector & vector);
}


#endif //DISPLAY_DOUBLE_VECTOR_H
