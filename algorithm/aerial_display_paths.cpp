#include "aerial_display_paths.h"

std::ostream & aed::operator<<(std::ostream &os, const aerial_frame_paths &frame_paths) {
    os << "Total cost: " << frame_paths.total_cost << std::endl;
    os << "Max cost: " << frame_paths.max_cost << std::endl;

    for (int i = 0; i < frame_paths.drones.size(); i++) {
        const aerial_frame_drone &drone = frame_paths.drones[i];
        os << "Agent " << i << ", total total_cost: " << drone.path->cost << std::endl;
        os << "Color: " << drone.from_color << " -> " << drone.to_color << std::endl;

        for (auto & node : drone.path->path) {
            os << "===" << std::endl;
            os << "Vertex: " << node.vertex << std::endl;
            os << "Total time:" << node.total_time << std::endl;
            os << "Speed:" << node.speed << std::endl;
            os << "Length:" << node.length << std::endl;
            os << "Start acceleration:" << std::endl;
            os << node.start_acceleration << std::endl;
            os << "End acceleration:" << std::endl;
            os << node.end_acceleration << std::endl;
            os << "t1: " << node.t1 << std::endl;
            os << "t2: " << node.t2 << std::endl;
            os << "t3: " << node.t3 << std::endl;
        }
        os << "===" << std::endl;
    }

    return os;
}