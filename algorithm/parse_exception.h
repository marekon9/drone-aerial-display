#ifndef DISPLAY_PARSE_EXCEPTION_H
#define DISPLAY_PARSE_EXCEPTION_H

#include <string>
#include <exception>

namespace aed {
class parse_exception : public std::exception {
    private:
        int line;
        std::string content;
        std::string reason;

    public:
        parse_exception(int line, const std::string& content, const std::string& reason);

        int get_line() const;

        const std::string& get_content() const;

        const char *what() const noexcept override;

        const std::string& get_reason() const;
    };
}


#endif //DISPLAY_PARSE_EXCEPTION_H
