#include "box_occupation.h"

bool aed::box_occupation::operator==(const aed::box_occupation &oth) const noexcept {
    return time == oth.time &&
           box == oth.box;
}

bool aed::box_occupation::operator!=(const aed::box_occupation &oth) const noexcept {
    return !(oth == *this);
}