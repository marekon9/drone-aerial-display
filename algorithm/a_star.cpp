#include "a_star.h"

#include <utility>
#include <cmath>
#include <unordered_set>
#include <memory>
#include <queue>

struct aed::a_star::path_node {
    std::shared_ptr<path_node> prev;
    coordinates direction;
    coordinates coord;
    double cost;
    double speed;
    double length;
    path_res_acceleration start_acc;
    path_res_acceleration end_acc;
    double_vector t1;
    double_vector t2;
    double_vector t3;
    int pos;

    path_node(std::shared_ptr<path_node> prev, const coordinates &direction, const coordinates &coord, double cost,
              double length, double speed, const path_res_acceleration &start_acc, const path_res_acceleration &end_acc,
              const double_vector &t1, const double_vector &t2,
              const double_vector &t3, int pos)
            : prev(std::move(prev)), direction(direction), coord(coord), cost(cost), length(length), speed(speed), start_acc(start_acc),
              end_acc(end_acc), t1(t1), t2(t2), t3(t3), pos(pos) {}

    aed::path_result to_path_result() const {
        std::vector<path_result_node> path;
        path.resize(pos + 1, path_result_node(0, 0, 0, {0, 0}, {0, 0}, {}, {}, {}, {}));

        const path_node* last = nullptr;
        const path_node* ptr = this;
        while (ptr != nullptr) {
            path[ptr->pos] = path_result_node(ptr->cost, ptr->speed, ptr->length, ptr->start_acc, ptr->end_acc,
                                              ptr->coord, ptr->t1, ptr->t2, ptr->t3);
            last = ptr;
            ptr = ptr->prev.get();
        }

        return aed::path_result(cost, std::move(path));
    }
};

aed::a_star::a_star(aed::graph  graph) : _graph(std::move(graph)) {}

std::unique_ptr<aed::path_result> aed::a_star::find_path(int waitRound, const aed::conflict_resolver & conflicts, const aed::coordinates &from, const aed::coordinates &to) {
    if (from == to) {
        return std::make_unique<aed::path_result>(aed::path_result(0, {path_result_node(0, 0, 0, {0, 0}, {0, 0}, from,
                                                                                        {},
                                                                                        {},
                                                                                        {})}));
    }

    double grid_coef = _graph.getSpace() / _graph.getMaxSpeed();
    double distance_multiplier = _graph.getDistanceMultiplier();
    auto comp = [grid_coef, distance_multiplier, to](const std::shared_ptr<aed::a_star::path_node>& a, const std::shared_ptr<aed::a_star::path_node>& b) {
        double a_estimate = a->cost + a->direction.distance(coordinates()) * grid_coef + (a->coord + a->direction).distance(to) * grid_coef * distance_multiplier;
        double b_estimate = b->cost + b->direction.distance(coordinates()) * grid_coef + (b->coord + b->direction).distance(to) * grid_coef * distance_multiplier;

        return a_estimate > b_estimate;
    };

    std::priority_queue<std::shared_ptr<aed::a_star::path_node>, std::vector<std::shared_ptr<aed::a_star::path_node>>, decltype(comp)> queue(comp);
    auto init = std::make_shared<aed::a_star::path_node>(nullptr, coordinates(), from, 0.0,
                                                                                      0.0, 0.0,
                                                                                      path_res_acceleration(),
                                                                                      path_res_acceleration(),
                                                                                      double_vector(), double_vector(),
                                                                                      double_vector(), 0);
    if (waitRound > 0) {
        auto wait = std::make_shared<path_node>(init, coordinates(), from, waitRound * _graph.getBatchDelay(), 0.0, 0.0,
                                                path_res_acceleration(), path_res_acceleration(), double_vector(),
                                                double_vector(), double_vector(), 1);
        queue.push(wait);
    } else {
        queue.push(init);
    }

    std::vector<box_occupation> trajectory;
    std::vector<neighbour> neighbors;

    std::unordered_set<aed::coordinates> visited;
    visited.insert(from);

    std::unordered_set<coordinates> visited_at_the_moment;
    std::unordered_map<coordinates, std::shared_ptr<path_node>> wait_map;

    while (!queue.empty()) {
        auto element_ptr = queue.top();
        queue.pop();

        visited_at_the_moment.clear();
        wait_map.clear();

        iterate_over_neighbors<decltype(comp), false>(conflicts, queue, visited_at_the_moment,
                                                      trajectory,
                                                      _graph.get_neighborhood_in_direction(element_ptr->direction),
                                                      visited, element_ptr, from, to, wait_map);
        std::unique_ptr<aed::path_result> res = iterate_over_neighbors<decltype(comp), true>(conflicts, queue,
                                                                                             visited_at_the_moment,
                                                                                             trajectory,
                                                                                             _graph.get_stop_neighborhood_in_direction(
                                                                                                     neighbors,
                                                                                                     element_ptr->direction),
                                                                                             visited, element_ptr, from,
                                                                                             to, wait_map);
        if (res != nullptr) {
            return res;
        }

        for (const auto & wait : wait_map) {
            queue.push(wait.second);
        }
    }

    return nullptr;
}

template<typename T, bool stop>
std::unique_ptr<aed::path_result> aed::a_star::iterate_over_neighbors(const aed::conflict_resolver &conflicts,
                                                                      std::priority_queue<std::shared_ptr<aed::a_star::path_node>, std::vector<std::shared_ptr<aed::a_star::path_node>>, T> &queue,
                                                                      std::unordered_set<aed::coordinates> &visited_at_the_moment,
                                                                      std::vector<box_occupation> &trajectory,
                                                                      const std::vector<neighbour> &neighbors,
                                                                      std::unordered_set<aed::coordinates> &visited,
                                                                      const std::shared_ptr<aed::a_star::path_node> &element_ptr,
                                                                      const aed::coordinates &from,
                                                                      const aed::coordinates &to,
                                                                      std::unordered_map<aed::coordinates, std::shared_ptr<a_star::path_node>> &wait_map) {
    for (const auto & neighbor : neighbors) {
        coordinates neighbor_coord = element_ptr->coord + neighbor.coord;
        if (! _graph.is_in_graph(neighbor_coord)) {
            continue;
        }

        path_res_acceleration acc(0, _graph.getMaxAcceleration());
        path_res_acceleration deacc(0, -_graph.getMaxAcceleration());

        double rest_speed = this->_graph.getMaxSpeed();
        double rest_time = 0;

        double total_time;
        double conflict_duration = 0;

        std::unordered_set<coordinates> conflict_set;
        bool conflict_ok = true;

        do {
            acc.duration = (rest_speed - element_ptr->speed) / acc.acceleration;

            if constexpr(stop) {
                deacc.duration = -rest_speed / deacc.acceleration;
                rest_time = (neighbor.length - acc.get_distance(element_ptr->speed) - deacc.get_distance(rest_speed)) / rest_speed;

                if (rest_time < 0) {
                    acc.duration = compute_cross_acceleration_time(acc.acceleration, deacc.acceleration, element_ptr->speed, 0, neighbor.length);
                    rest_speed = acc.duration * acc.acceleration + element_ptr->speed;
                    deacc.duration = -rest_speed / deacc.acceleration;
                    rest_time = 0;
                }
            } else {
                rest_time = (neighbor.length - acc.get_distance(element_ptr->speed)) / rest_speed;
            }

            total_time = acc.duration + rest_time + deacc.duration;

            _graph.find_trajectory(trajectory, neighbor, element_ptr->speed, total_time, acc, deacc);

            box_occupation conflict;
            for (const auto & box : trajectory) {
                time_interval conflict_time(box.time.from + element_ptr->cost, box.time.to + element_ptr->cost);
                const coordinates box_coordinates = element_ptr->coord + box.box;
                double duration = conflicts.conflict_duration(conflict_time, box_coordinates);

                if (duration > conflict_duration) {
                    conflict_duration = duration;
                    conflict = box;
                }
            }

            if (conflict_duration > 0) {
                conflict_ok = conflict_set.insert(conflict.box).second;
                acc.acceleration = (2 * (conflict.time.to - element_ptr->speed * conflict.time.to)) / (conflict.time.to * conflict.time.to);
                rest_speed = acc.acceleration * conflict.time.to + element_ptr->speed;
            }
        } while (conflict_duration > 0 && rest_time >= 0 && rest_speed > 0 && _graph.getMaxAcceleration() - std::abs(acc.acceleration) >= 0 && conflict_ok);

        if (conflict_duration > 0) {
            if (element_ptr->speed == 0 && conflict_duration < std::numeric_limits<double>::infinity() && element_ptr->coord != from) {
                auto it = wait_map.find(neighbor.coord);
                double cost = element_ptr->cost + conflict_duration;
                if (it == wait_map.end() || cost > it->second->cost) {
                    wait_map.insert(std::pair(neighbor.coord, std::make_shared<path_node>(element_ptr, neighbor.coord, element_ptr->coord, cost,0, 0,
                                                                                          path_res_acceleration(), path_res_acceleration(), double_vector(), double_vector(), double_vector(), element_ptr->pos + 1)));
                }
            }
            continue;
        }
        if constexpr(stop) {
            rest_speed = 0;
        }

        if (neighbor_coord == to) {
            if constexpr (stop) {
                path_node node(element_ptr, coordinates(), neighbor_coord, element_ptr->cost + total_time, neighbor.length,
                               rest_speed, acc, deacc, {}, {}, {}, element_ptr->pos + 1);
                return std::make_unique<aed::path_result>(node.to_path_result());
            } else {
                continue;
            }
        }

        bool initiated = false;
        if (visited.insert(neighbor_coord).second || (initiated = visited_at_the_moment.find(neighbor_coord) != visited_at_the_moment.end())) {
            if (!initiated) {
                visited_at_the_moment.insert(neighbor_coord);
            }
            coordinates from_dir;
            if (!stop) {
                from_dir = neighbor.continue_dir;
            }

            auto to_push = std::make_shared<path_node>(element_ptr, from_dir, neighbor_coord, element_ptr->cost + total_time,
                                                   neighbor.length, rest_speed, acc, deacc,
                                                   neighbor.t, neighbor.t2, neighbor.t3, element_ptr->pos + 1);
            if constexpr (stop) {
                if (_graph.getStopPenalty() > 0) {
                    to_push = std::make_shared<path_node>(to_push, coordinates(), neighbor_coord, to_push->cost + _graph.getStopPenalty(),
                                                          0, 0, path_res_acceleration(), path_res_acceleration(), double_vector(), double_vector(), double_vector(), to_push->pos + 1);
                }
            }
            queue.push(std::move(to_push));
        }
    }
    return nullptr;
}

double aed::a_star::compute_cross_acceleration_time(double acc1, double acc2, double init_speed, double final_speed,
                                                    double total_distance) {
    double acc1_2 = acc1 * acc1;
    double a = acc1 * acc2 - acc1_2;

    double b = 2 * init_speed * acc2 - 2 * init_speed * acc1;

    double c = final_speed * final_speed - init_speed * init_speed - 2 * total_distance * acc2;

    double discriminant = std::sqrt(b * b - 4 * a * c);
    return std::max((-b -discriminant) / (2 * a), (-b + discriminant) / (2 * a));
}

