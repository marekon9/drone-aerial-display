#include "frame.h"

aed::frame::frame(const std::vector<drone_position> & positions) : positions(positions) {}

aed::frame::frame(std::vector<drone_position> &&positions): positions(std::move(positions)) {}
