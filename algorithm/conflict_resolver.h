#ifndef DISPLAY_CONFLICT_RESOLVER_H
#define DISPLAY_CONFLICT_RESOLVER_H

#include "box_occupation.h"
#include <unordered_set>
#include <map>
#include <unordered_map>

namespace aed {
    class conflict_resolver {
    private:
    public:
        std::unordered_map<coordinates, std::multimap<double, box_occupation>> conflicts;
    public:

        void add_conflict(const box_occupation & conflict);

        double conflict_duration(time_interval time, const coordinates & box) const noexcept;
    };
}


#endif //DISPLAY_CONFLICT_RESOLVER_H
