#ifndef DISPLAY_AED_LIGHT_POINT_H
#define DISPLAY_AED_LIGHT_POINT_H

#include <Godot.hpp>
#include <Object.hpp>
#include <Color.hpp>

namespace godot {
    class aed_light_point : public Object {
        GODOT_CLASS(aed_light_point, Object)
    private:
        float from_time;
        Color color;
    public:
        aed_light_point();

        void _init();

        float get_from_time() const noexcept;

        void set_from_time(float from_time) noexcept;

        Color get_color() const;

        void set_color(Color color);

        static void _register_methods();
    };
}


#endif //DISPLAY_AED_LIGHT_POINT_H
