#ifndef DISPLAY_AED_AGENT_H
#define DISPLAY_AED_AGENT_H

#include <Godot.hpp>
#include <Array.hpp>
#include <Object.hpp>

namespace godot {
    class aed_agent : public Object {
        GODOT_CLASS(aed_agent, Object)
    private:
        Array path;
        Array light_path;
    public:
        void _init();

        Array get_path() const noexcept;

        Array& get_path_ref();

        void set_path(Array path) noexcept;

        Array get_light_path() const noexcept;

        Array& get_light_path_ref() noexcept;

        void set_light_path(Array light_path) noexcept;

        static void _register_methods();
    };
}


#endif //DISPLAY_AED_AGENT_H
