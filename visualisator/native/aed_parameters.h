#ifndef DISPLAY_AED_PARAMETERS_H
#define DISPLAY_AED_PARAMETERS_H

#include <Godot.hpp>
#include <Object.hpp>
#include <Array.hpp>
#include <String.hpp>
#include <memory>
#include "../../../algorithm/parameters.h"
#include "aed_graph_settings.h"

namespace godot {
    class aed_parameters : public Object {
        GODOT_CLASS(aed_parameters, Object)
    private:
        aed_graph_settings * settings = nullptr;
        int drone_count;
        double wait_time;
        Array frames;
        bool dirty;
        std::unique_ptr<aed::parameters> params = nullptr;

        void reload_frames_from_inner();

        static Color transform_color(const aed::color & color);

    public:

        aed_parameters();

        virtual ~aed_parameters();

        void _init();

        static void _register_methods();

        bool load_from_file(String location);

        bool store_to_file(String location, bool include_path);

        bool reload_inner_parameters(std::vector<std::unique_ptr<aed::aerial_frame_paths>> && path);

        bool has_paths();

        Array get_paths();

        const aed::parameters* getParams() const;

        double getWaitTime() const;

        void setWaitTime(double waitTime);

        aed_graph_settings *getSettings() const;

        void setSettings(aed_graph_settings *settings);

        Array getFrames() const;

        void setFrames(Array frames);

        int getDroneCount() const;

        void setDroneCount(int droneCount);

        bool isDirty() const;

        void setDirty(bool dirty);
    };

}


#endif //DISPLAY_AED_PARAMETERS_H
