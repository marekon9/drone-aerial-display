#include "aed_frame.h"
#include "aed_drone_position.h"


void godot::aed_frame::_init() {
    //empty
}

void godot::aed_frame::_register_methods() {
    register_property<godot::aed_frame, Array>("positions", &aed_frame::set_positions, &aed_frame::get_positions, Array());
}

godot::Array godot::aed_frame::get_positions() const noexcept {
    return positions;
}

void godot::aed_frame::set_positions(godot::Array positions) noexcept {
    this->positions = positions;
}
