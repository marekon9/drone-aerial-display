#ifndef DISPLAY_AED_DRONE_POSITION_H
#define DISPLAY_AED_DRONE_POSITION_H

#include <Godot.hpp>
#include <Object.hpp>
#include <Color.hpp>

namespace godot {
    class aed_drone_position : public Object {
        GODOT_CLASS(aed_drone_position, Object)
    private:
        int x;
        int y;
        int z;
        Color color;
        int agent;
    public:
        aed_drone_position();

        static void _register_methods();

        void _init();

        int get_x() const;

        void set_x(int x);

        int get_y() const;

        void set_y(int y);

        int get_z() const;

        void set_z(int z);

        Color get_color() const;

        void set_color(Color color);

        int get_agent() const;

        void set_agent(int agent);
    };
}


#endif //DISPLAY_AED_DRONE_POSITION_H
