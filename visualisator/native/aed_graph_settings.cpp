#include "aed_graph_settings.h"

godot::aed_graph_settings::aed_graph_settings() : length(10), width(10), height(10), k(3), precision(100), curvature(1),
                                                  grid_size(1000), drone_count(1), drone_length(300), drone_width(300),
                                                  drone_height(500), max_speed(0), max_acceleration(0), stop_penalty(0),
                                                  distance_multiplier(10), batches(30), batch_delay(3000), batch_distance(3) {}

void godot::aed_graph_settings::_init() {
    // empty
}

aed::graph godot::aed_graph_settings::to_graph() {
    return aed::graph(length, width, height, k, precision, curvature, grid_size, drone_length, drone_width,
                      drone_height, max_speed, max_acceleration, stop_penalty, distance_multiplier, batches, batch_delay, batch_distance);
}

void godot::aed_graph_settings::from_graph(const aed::graph &graph) {
    length = graph.getLength();
    width = graph.getWidth();
    height = graph.getHeight();
    k = graph.getK();
    precision = graph.getPrecision();
    curvature = graph.getCurvature();
    grid_size = graph.getSpace();

    drone_length = graph.getElipsoidA();
    drone_width = graph.getElipsoidB();
    drone_height = graph.getElipsoidC();

    max_speed = graph.getMaxSpeed();
    max_acceleration = graph.getMaxAcceleration();

    stop_penalty = graph.getStopPenalty();
    distance_multiplier = graph.getDistanceMultiplier();

    batches = graph.getBatches();
    batch_delay = graph.getBatchDelay();
    batch_distance = graph.getBatchDistance();
}

void godot::aed_graph_settings::_register_methods() {
    register_property<aed_graph_settings, int>("length", &aed_graph_settings::setLength, &aed_graph_settings::getLength, 10);
    register_property<aed_graph_settings, int>("width", &aed_graph_settings::setWidth, &aed_graph_settings::getWidth, 10);
    register_property<aed_graph_settings, int>("height", &aed_graph_settings::setHeight, &aed_graph_settings::getHeight, 10);
    register_property<aed_graph_settings, int>("k", &aed_graph_settings::setK, &aed_graph_settings::getK, 3);
    register_property<aed_graph_settings, int>("precision", &aed_graph_settings::setPrecision, &aed_graph_settings::getPrecision, 100);
    register_property<aed_graph_settings, double>("curvature", &aed_graph_settings::setCurvature, &aed_graph_settings::getCurvature, 1.0);
    register_property<aed_graph_settings, double>("grid_size", &aed_graph_settings::setGridSize, &aed_graph_settings::getGridSize, 1000.0);

    register_property<aed_graph_settings, int>("drone_count", &aed_graph_settings::setDroneCount, &aed_graph_settings::getDroneCount, 1);
    register_property<aed_graph_settings, double>("drone_length", &aed_graph_settings::setDroneLength, &aed_graph_settings::getDroneLength, 300.0);
    register_property<aed_graph_settings, double>("drone_width", &aed_graph_settings::setDroneWidth, &aed_graph_settings::getDroneWidth, 300.0);
    register_property<aed_graph_settings, double>("drone_height", &aed_graph_settings::setDroneHeight, &aed_graph_settings::getDroneHeight, 500.0);

    register_property<aed_graph_settings, double>("max_speed", &aed_graph_settings::setMaxSpeed, &aed_graph_settings::getMaxSpeed, 5);
    register_property<aed_graph_settings, double>("max_acceleration", &aed_graph_settings::setMaxAcceleration, &aed_graph_settings::getMaxAcceleration, 1);

    register_property<aed_graph_settings, double>("stop_penalty", &aed_graph_settings::setStopPenalty, &aed_graph_settings::getStopPenalty, 0.0);
    register_property<aed_graph_settings, double>("distance_multiplier", &aed_graph_settings::setDistanceMultiplier, &aed_graph_settings::getDistanceMultiplier, 10.0);

    register_property<aed_graph_settings, int>("batches", &aed_graph_settings::setBatches, &aed_graph_settings::getBatches, 30);
    register_property<aed_graph_settings, double>("batch_delay", &aed_graph_settings::setBatchDelay, &aed_graph_settings::getBatchDelay, 3000.0);
    register_property<aed_graph_settings, double>("batch_distance", &aed_graph_settings::setBatchDistance, &aed_graph_settings::getBatchDistance, 3.0);
}

// Getters & Setters

int godot::aed_graph_settings::getLength() const {
    return length;
}

void godot::aed_graph_settings::setLength(int length) {
    aed_graph_settings::length = length;
}

int godot::aed_graph_settings::getWidth() const {
    return width;
}

void godot::aed_graph_settings::setWidth(int width) {
    aed_graph_settings::width = width;
}

int godot::aed_graph_settings::getHeight() const {
    return height;
}

void godot::aed_graph_settings::setHeight(int height) {
    aed_graph_settings::height = height;
}

int godot::aed_graph_settings::getK() const {
    return k;
}

void godot::aed_graph_settings::setK(int k) {
    aed_graph_settings::k = k;
}

double godot::aed_graph_settings::getGridSize() const {
    return grid_size;
}

void godot::aed_graph_settings::setGridSize(double gridSize) {
    grid_size = gridSize;
}

double godot::aed_graph_settings::getDroneLength() const {
    return drone_length;
}

void godot::aed_graph_settings::setDroneLength(double droneLength) {
    drone_length = droneLength;
}

double godot::aed_graph_settings::getDroneWidth() const {
    return drone_width;
}

void godot::aed_graph_settings::setDroneWidth(double droneWidth) {
    drone_width = droneWidth;
}

double godot::aed_graph_settings::getDroneHeight() const {
    return drone_height;
}

void godot::aed_graph_settings::setDroneHeight(double droneHeight) {
    drone_height = droneHeight;
}

int godot::aed_graph_settings::getDroneCount() const {
    return drone_count;
}

void godot::aed_graph_settings::setDroneCount(int droneCount) {
    this->drone_count = droneCount;
}

int godot::aed_graph_settings::getPrecision() const {
    return precision;
}

void godot::aed_graph_settings::setPrecision(int precision) {
    aed_graph_settings::precision = precision;
}

double godot::aed_graph_settings::getCurvature() const {
    return curvature;
}

void godot::aed_graph_settings::setCurvature(double curvature) {
    aed_graph_settings::curvature = curvature;
}

double godot::aed_graph_settings::getMaxSpeed() const {
    return max_speed;
}

void godot::aed_graph_settings::setMaxSpeed(double maxSpeed) {
    max_speed = maxSpeed;
}

double godot::aed_graph_settings::getMaxAcceleration() const {
    return max_acceleration;
}

void godot::aed_graph_settings::setMaxAcceleration(double maxAcceleration) {
    max_acceleration = maxAcceleration;
}

double godot::aed_graph_settings::getStopPenalty() const {
    return stop_penalty;
}

void godot::aed_graph_settings::setStopPenalty(double stopPenalty) {
    stop_penalty = stopPenalty;
}

double godot::aed_graph_settings::getDistanceMultiplier() const {
    return distance_multiplier;
}

void godot::aed_graph_settings::setDistanceMultiplier(double distanceMultiplier) {
    distance_multiplier = distanceMultiplier;
}

int godot::aed_graph_settings::getBatches() const {
    return batches;
}

void godot::aed_graph_settings::setBatches(int batches) {
    aed_graph_settings::batches = batches;
}

double godot::aed_graph_settings::getBatchDelay() const {
    return batch_delay;
}

void godot::aed_graph_settings::setBatchDelay(double batchDelay) {
    batch_delay = batchDelay;
}

double godot::aed_graph_settings::getBatchDistance() const {
    return batch_distance;
}

void godot::aed_graph_settings::setBatchDistance(double batchDistance) {
    batch_distance = batchDistance;
}

