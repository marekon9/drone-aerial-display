#include "aed_parameters.h"

#include <fstream>
#include <sstream>
#include "../../../algorithm/parse_exception.h"
#include "aed_agent.h"
#include "aed_point.h"
#include "aed_light_point.h"
#include "aed_frame.h"
#include "aed_drone_position.h"

godot::aed_parameters::aed_parameters() : settings(aed_graph_settings::_new()), drone_count(1), wait_time(3000.0), frames(), dirty(
        true), params(nullptr) {}

void godot::aed_parameters::_init() {
    // empty
}

void godot::aed_parameters::_register_methods() {
    register_property<aed_parameters, aed_graph_settings*>("settings", &aed_parameters::setSettings, &aed_parameters::getSettings,
                                                           nullptr);
    register_property<aed_parameters, int>("drone_count", &aed_parameters::setDroneCount, &aed_parameters::getDroneCount, 1);
    register_property<aed_parameters, double>("wait_time", &aed_parameters::setWaitTime, &aed_parameters::getWaitTime, 3000.0);
    register_property<aed_parameters, Array>("frames", &aed_parameters::setFrames, &aed_parameters::getFrames, Array());
    register_property<aed_parameters, bool>("dirty", &aed_parameters::setDirty, &aed_parameters::isDirty, true);

    register_method("load_from_file", &aed_parameters::load_from_file);
    register_method("store_to_file", &aed_parameters::store_to_file);
    register_method("has_path", &aed_parameters::has_paths);
    register_method("get_paths", &aed_parameters::get_paths);

    register_signal<aed_parameters>("parse_error", "line_num", GODOT_VARIANT_TYPE_INT, "line", GODOT_VARIANT_TYPE_STRING, "reason", GODOT_VARIANT_TYPE_STRING);
    register_signal<aed_parameters>("general_error", "reason", GODOT_VARIANT_TYPE_STRING);
}

bool godot::aed_parameters::load_from_file(godot::String location) {
    std::unique_ptr<aed::parameters> backup(std::move(params));
    try {
        std::ifstream file(location.alloc_c_string());
        params = std::make_unique<aed::parameters>(file);

        this->settings->from_graph(params->get_graph());
        reload_frames_from_inner();
        this->wait_time = params->get_wait_time();
        this->drone_count = params->get_drone_count();
        this->dirty = false;

        return true;
    } catch (aed::parse_exception & e) {
        emit_signal("parse_error", e.get_line(), e.get_content().c_str(), e.get_reason().c_str());
    } catch (std::exception & e) {
        emit_signal("general_error", e.what());
    }
    params = std::move(backup);
    return false;
}

void godot::aed_parameters::reload_frames_from_inner() {
    frames = Array();

    for (const auto & frame : params->get_frames()) {
        aed_frame * godot_frame = aed_frame::_new();
        frames.append(Variant(godot_frame));

        Array godot_positions;

        for (const auto & position : frame.positions) {
            aed_drone_position * godot_position = aed_drone_position::_new();
            godot_positions.push_back(Variant(godot_position));

            godot_position->set_agent(position.agent);
            godot_position->set_color(transform_color(position.color));
            godot_position->set_x(position.position.x);
            godot_position->set_y(position.position.y);
            godot_position->set_z(position.position.z);
        }

        godot_frame->set_positions(godot_positions);
    }
}

bool godot::aed_parameters::store_to_file(godot::String location, bool include_path) {
    if (dirty || params == nullptr) {
        bool reloaded = reload_inner_parameters({});
        if (!reloaded) {
            return false;
        }
    }

    try {
        std::ofstream file(location.alloc_c_string());
        params->save(file, include_path);
        return true;
    } catch (aed::parse_exception & e) {
        emit_signal("parse_error", e.get_line(), e.get_content().c_str(), e.get_reason().c_str());
    } catch (std::exception & e) {
        emit_signal("general_error", e.what());
    }
    return false;
}

bool godot::aed_parameters::reload_inner_parameters(std::vector<std::unique_ptr<aed::aerial_frame_paths>> && path) {
    std::ostringstream oss;
    std::vector<aed::frame> frame_input;

    int frame_size = frames.size();
    for (int i = 0; i < frame_size; ++i) {
        aed_frame * frame = frames[i].operator aed_frame*();
        if (frame == nullptr) {
            oss << "Missing frame " << i << " / " << (frame_size - 1) << ".";
            emit_signal("general_error", oss.str().c_str());
            return false;
        }

        Array positions = frame->get_positions();
        if (positions.size() != drone_count) {
            oss << "Frame " << i << " / " << (frame_size - 1) << " has unexpected size of drone positions. Expected " << drone_count << ", got " << positions.size() << " instead.";
            emit_signal("general_error", oss.str().c_str());
            return false;
        }

        std::vector<aed::drone_position> drone_positions;
        drone_positions.reserve(drone_count);

        for (int j = 0; j < drone_count; j++) {
            aed_drone_position* position = positions[j].operator aed_drone_position*();
            if (position == nullptr) {
                oss << "Drone position with index " << j << " / " << (drone_count - 1) << " is missing in frame " << i << " / " << (frame_size - 1) << ".";
                emit_signal("general_error", oss.str().c_str());
                return false;
            }
            Color color = position->get_color();
            drone_positions.emplace_back(aed::coordinates(position->get_x(), position->get_y(), position->get_z()),
                                         aed::color{color.get_r8(), color.get_g8(), color.get_b8(), color.get_a8() == 255},
                                         position->get_agent()
            );
        }

        frame_input.emplace_back(std::move(drone_positions));
    }

    try {
        std::unique_ptr<aed::parameters> new_params = std::make_unique<aed::parameters>(settings->to_graph(), drone_count, wait_time, std::move(frame_input), std::move(path));
        dirty = false;
        params = std::move(new_params);
    } catch (std::exception & e) {
        emit_signal("general_error", e.what());
        return false;
    }

    return true;
}

godot::Array godot::aed_parameters::get_paths() {
    if (params == nullptr) {
        return Array();
    }

    Array all_paths;

    for (int i = 0; i < drone_count; ++i) {
        all_paths.push_back(Variant(aed_agent::_new()));
    }

    double middle = settings->getGridSize() / 2;
    double from_time = 0.0;
    int frame = 0;
    for (auto & path : params->get_frame_paths()) {
        if (path == nullptr) {
            std::ostringstream oss;
            oss << "Paths from frame " << frame << " to " << (frame + 1) << " could not be computed. Try again or change settings and then try again.";
            emit_signal("general_error", oss.str().c_str());
            return Array();
        }
        frame++;

        Array agent_paths;
        int i = 0;
        for (const auto & drone : path->drones) {
            aed_agent * res_agent = all_paths[i++].operator aed_agent*();
            for (const auto & path_element : drone.path->path) {
                aed_point * point = aed_point::_new();
                point->set_from_time(from_time + path_element.total_time);
                point->set_x((path_element.vertex.x * settings->getGridSize() + middle) / 1000);
                point->set_y((path_element.vertex.z * settings->getGridSize() + middle) / 1000);
                point->set_z((path_element.vertex.y * settings->getGridSize() + middle) / 1000);
                point->set_length(path_element.length);
                point->set_speed(path_element.speed);
                auto start_acc = point->get_start_acceleration();
                start_acc->setAcceleration(path_element.start_acceleration.acceleration);
                start_acc->setDuration(path_element.start_acceleration.duration);

                auto end_acc = point->get_end_acceleration();
                end_acc->setAcceleration(path_element.end_acceleration.acceleration);
                end_acc->setDuration(path_element.end_acceleration.duration);

                point->set_t1(Vector3(path_element.t1.x, path_element.t1.z, path_element.t1.y));
                point->set_t2(Vector3(path_element.t2.x, path_element.t2.z, path_element.t2.y));
                point->set_t3(Vector3(path_element.t3.x, path_element.t3.z, path_element.t3.y));

                res_agent->get_path_ref().append(Variant(point));
            }

            aed_light_point * light_point = aed_light_point::_new();
            light_point->set_from_time(from_time);
            light_point->set_color(transform_color(drone.from_color));
            res_agent->get_light_path_ref().push_back(Variant(light_point));

            aed_light_point * to_light = aed_light_point::_new();
            to_light->set_from_time(from_time + path->max_cost);
            to_light->set_color(transform_color(drone.to_color));
            res_agent->get_light_path_ref().push_back(Variant(to_light));
        }

        from_time += path->max_cost + wait_time;
    }

    auto last = params->get_frame_paths()[params->get_frame_paths().size() - 1].get();
    for (int i = 0; i < drone_count; ++i) {
        aed_agent * agent = all_paths[i].operator aed_agent*();

        aed_light_point * lightPoint = aed_light_point::_new();
        lightPoint->set_from_time(from_time);
        lightPoint->set_color(transform_color(last->drones[i].to_color));
        agent->get_light_path_ref().push_back(lightPoint);
    }
    return all_paths;
}

godot::Color godot::aed_parameters::transform_color(const aed::color &color) {
    return godot::Color(
            (float) color.red / 255.f,
            (float) color.green / 255.f,
            (float) color.blue / 255.f,
            color.active ? 1.f : 0.f
    );
}


bool godot::aed_parameters::has_paths() {
    return params != nullptr && !dirty && !params->get_frame_paths().empty();
}

godot::aed_graph_settings *godot::aed_parameters::getSettings() const {
    return settings;
}

void godot::aed_parameters::setSettings(godot::aed_graph_settings *settings) {
    aed_parameters::settings = settings;
}

godot::Array godot::aed_parameters::getFrames() const {
    return frames;
}

void godot::aed_parameters::setFrames(godot::Array frames) {
    this->frames = frames;
}

godot::aed_parameters::~aed_parameters() {
    if (settings != nullptr) {
        settings->free();
    }
}

double godot::aed_parameters::getWaitTime() const {
    return wait_time;
}

void godot::aed_parameters::setWaitTime(double waitTime) {
    wait_time = waitTime;
}

int godot::aed_parameters::getDroneCount() const {
    return drone_count;
}

void godot::aed_parameters::setDroneCount(int droneCount) {
    drone_count = droneCount;
}

bool godot::aed_parameters::isDirty() const {
    return dirty;
}

void godot::aed_parameters::setDirty(bool dirty) {
    aed_parameters::dirty = dirty;
}

const aed::parameters * godot::aed_parameters::getParams() const {
    return params.get();
}

