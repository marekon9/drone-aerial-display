#include "aed_light_point.h"

godot::aed_light_point::aed_light_point() : from_time(0.0f), color() {}

void godot::aed_light_point::_init() {
    //empty
}

void godot::aed_light_point::_register_methods() {
    register_property<aed_light_point, float>("from_time", &aed_light_point::set_from_time, &aed_light_point::get_from_time, 0.0f);
    register_property<aed_light_point, Color>("color", &aed_light_point::set_color, &aed_light_point::get_color, Color());
}

float godot::aed_light_point::get_from_time() const noexcept {
    return from_time;
}

void godot::aed_light_point::set_from_time(float from_time) noexcept {
    this->from_time = from_time;
}

godot::Color godot::aed_light_point::get_color() const {
    return color;
}

void godot::aed_light_point::set_color(godot::Color color) {
    this->color = color;
}
