#ifndef DISPLAY_AED_POINT_H
#define DISPLAY_AED_POINT_H

#include <Godot.hpp>
#include <Object.hpp>
#include "aed_acceleration.h"

namespace godot {
    class aed_point : public Object {
        GODOT_CLASS(aed_point, Object)
    private:
        float from_time;
        float x;
        float y;
        float z;
        float speed;
        float length;
        aed_acceleration * start_acceleration;
        aed_acceleration * end_acceleration;
        Vector3 t1;
        Vector3 t2;
        Vector3 t3;
    public:
        aed_point();

        virtual ~aed_point();

        void _init() noexcept;

        float get_from_time() const noexcept;

        void set_from_time(float from_time) noexcept;

        float get_x() const noexcept;

        void set_x(float x) noexcept;

        float get_y() const noexcept;

        void set_y(float y) noexcept;

        float get_z() const noexcept;

        void set_z(float z) noexcept;

        aed_acceleration* get_start_acceleration() const;

        void set_start_acceleration(aed_acceleration *start_acceleration);

        aed_acceleration *get_end_acceleration() const;

        void set_end_acceleration(aed_acceleration *end_acceleration);

        float get_speed() const;

        void set_speed(float speed);

        float get_length() const;

        void set_length(float length);

        Vector3 get_t1() const;

        void set_t1(Vector3 t1);

        Vector3 get_t2() const;

        void set_t2(Vector3 t2);

        Vector3 get_t3() const;

        void set_t3(Vector3 t3);


        static void _register_methods();
    };
}

#endif //DISPLAY_AED_POINT_H
