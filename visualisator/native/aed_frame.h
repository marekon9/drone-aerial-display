#ifndef DISPLAY_AED_FRAME_H
#define DISPLAY_AED_FRAME_H

#include <Godot.hpp>
#include <Array.hpp>
#include <Object.hpp>

namespace godot {
    class aed_frame : public Object {
        GODOT_CLASS(aed_frame, Object)
    private:
        Array positions;
    public:
        void _init();

        Array get_positions() const noexcept;

        void set_positions(Array positions) noexcept;

        static void _register_methods();
    };
}


#endif //DISPLAY_AED_FRAME_H
