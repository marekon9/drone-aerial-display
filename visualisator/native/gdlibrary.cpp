#include "aed_point.h"
#include "aed_agent.h"
#include "aed_frame.h"
#include "aed_bridge.h"
#include "aed_drone_position.h"
#include "aed_light_point.h"
#include "aed_acceleration.h"

extern "C" void GDN_EXPORT godot_gdnative_init(godot_gdnative_init_options *o) {
    godot::Godot::gdnative_init(o);
}

extern "C" void GDN_EXPORT godot_gdnative_terminate(godot_gdnative_terminate_options *o) {
    godot::Godot::gdnative_terminate(o);
}

extern "C" void GDN_EXPORT godot_nativescript_init(void *handle) {
    godot::Godot::nativescript_init(handle);

    godot::register_class<godot::aed_graph_settings>();
    godot::register_class<godot::aed_acceleration>();
    godot::register_class<godot::aed_parameters>();
    godot::register_class<godot::aed_point>();
    godot::register_class<godot::aed_light_point>();
    godot::register_class<godot::aed_drone_position>();
    godot::register_class<godot::aed_frame>();
    godot::register_class<godot::aed_agent>();
    godot::register_class<godot::aed_bridge>();
}