#include "aed_drone_position.h"

godot::aed_drone_position::aed_drone_position(): x(0), y(0), z(0), color(), agent(-1) {

}

void godot::aed_drone_position::_init() {
    //empty
}

void godot::aed_drone_position::_register_methods() {
    register_property<aed_drone_position, int>("x", &aed_drone_position::set_x, &aed_drone_position::get_x, 0);
    register_property<aed_drone_position, int>("y", &aed_drone_position::set_y, &aed_drone_position::get_y, 0);
    register_property<aed_drone_position, int>("z", &aed_drone_position::set_z, &aed_drone_position::get_z, 0);
    register_property<aed_drone_position, Color>("color", &aed_drone_position::set_color, &aed_drone_position::get_color, Color());
    register_property<aed_drone_position, int>("agent", &aed_drone_position::set_agent, &aed_drone_position::get_agent, -1);
}

int godot::aed_drone_position::get_x() const {
    return x;
}

void godot::aed_drone_position::set_x(int x) {
    aed_drone_position::x = x;
}

int godot::aed_drone_position::get_y() const {
    return y;
}

void godot::aed_drone_position::set_y(int y) {
    aed_drone_position::y = y;
}

int godot::aed_drone_position::get_z() const {
    return z;
}

void godot::aed_drone_position::set_z(int z) {
    aed_drone_position::z = z;
}

godot::Color godot::aed_drone_position::get_color() const {
    return color;
}

void godot::aed_drone_position::set_color(godot::Color color) {
    aed_drone_position::color = color;
}

int godot::aed_drone_position::get_agent() const {
    return agent;
}

void godot::aed_drone_position::set_agent(int agent) {
    aed_drone_position::agent = agent;
}
