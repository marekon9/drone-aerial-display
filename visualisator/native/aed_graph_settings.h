#ifndef DISPLAY_AED_GRAPH_SETTINGS_H
#define DISPLAY_AED_GRAPH_SETTINGS_H

#include <Godot.hpp>
#include <Object.hpp>
#include "../../../algorithm/graph.h"

namespace godot {
    class aed_graph_settings : public Object {
        GODOT_CLASS(aed_graph_settings, Object)
    private:
        int length;
        int width;
        int height;
        int k;
        int precision;
        double curvature;

        int drone_count;
        double grid_size;

        double drone_length;
        double drone_width;
        double drone_height;

        double max_speed;
        double max_acceleration;

        double stop_penalty;
        double distance_multiplier;

        int batches;
        double batch_delay;
        double batch_distance;
    public:

        aed_graph_settings();

        void _init();

        static void _register_methods();

        aed::graph to_graph();

        void from_graph(const aed::graph & graph);

        int getLength() const;

        void setLength(int length);

        int getWidth() const;

        void setWidth(int width);

        int getHeight() const;

        void setHeight(int height);

        int getK() const;

        void setK(int k);

        double getGridSize() const;

        void setGridSize(double gridSize);

        int getDroneCount() const;

        void setDroneCount(int droneCount);

        double getDroneLength() const;

        void setDroneLength(double droneLength);

        double getDroneWidth() const;

        void setDroneWidth(double droneWidth);

        double getDroneHeight() const;

        void setDroneHeight(double droneHeight);

        int getPrecision() const;

        void setPrecision(int precision);

        double getCurvature() const;

        void setCurvature(double curvature);

        double getMaxSpeed() const;

        void setMaxSpeed(double maxSpeed);

        double getMaxAcceleration() const;

        void setMaxAcceleration(double maxAcceleration);

        double getStopPenalty() const;

        void setStopPenalty(double stopPenalty);

        double getDistanceMultiplier() const;

        void setDistanceMultiplier(double distanceMultiplier);

        int getBatches() const;

        void setBatches(int batches);

        double getBatchDelay() const;

        void setBatchDelay(double batchDelay);

        double getBatchDistance() const;

        void setBatchDistance(double batchDistance);
    };
}


#endif //DISPLAY_AED_GRAPH_SETTINGS_H
