#include "aed_acceleration.h"

godot::aed_acceleration::aed_acceleration() : acceleration(0), duration(0) {}

void godot::aed_acceleration::_register_methods() {
    register_property<aed_acceleration, double>("acceleration", &aed_acceleration::setAcceleration, &aed_acceleration::getAcceleration, 0.0);
    register_property<aed_acceleration, double>("duration", &aed_acceleration::setDuration, &aed_acceleration::getDuration, 0.0);
}

void godot::aed_acceleration::_init() {
    //empty
}

double godot::aed_acceleration::getAcceleration() const {
    return acceleration;
}

void godot::aed_acceleration::setAcceleration(double acceleration) {
    aed_acceleration::acceleration = acceleration;
}

double godot::aed_acceleration::getDuration() const {
    return duration;
}

void godot::aed_acceleration::setDuration(double duration) {
    aed_acceleration::duration = duration;
}
