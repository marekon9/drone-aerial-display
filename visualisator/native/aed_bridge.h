#ifndef DISPLAY_AED_BRIDGE_H
#define DISPLAY_AED_BRIDGE_H

#include <Godot.hpp>
#include <Object.hpp>
#include <Variant.hpp>
#include <Array.hpp>
#include <color.h>
#include "aed_graph_settings.h"
#include "aed_parameters.h"

namespace godot {
    class aed_bridge : public Object {
        GODOT_CLASS(aed_bridge, Object)

    public:

        void _init();

        static void _register_methods();

        bool compute_paths(aed_parameters * params);

    };
}


#endif //DISPLAY_AED_BRIDGE_H
