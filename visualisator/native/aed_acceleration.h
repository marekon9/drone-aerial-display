#ifndef DISPLAY_AED_ACCELERATION_H
#define DISPLAY_AED_ACCELERATION_H

#include <Godot.hpp>
#include <Object.hpp>

namespace godot {
    class aed_acceleration : public Object {
        GODOT_CLASS(aed_acceleration, Object)
    private:
        double acceleration;
        double duration;
    public:
        void _init();

        aed_acceleration();

        double getAcceleration() const;

        void setAcceleration(double acceleration);

        double getDuration() const;

        void setDuration(double duration);

        static void _register_methods();
    };
}



#endif //DISPLAY_AED_ACCELERATION_H
