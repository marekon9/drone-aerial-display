#include "aed_point.h"

godot::aed_point::aed_point() : from_time(0), x(0), y(0), z(0), length(0), speed(0),start_acceleration(nullptr), end_acceleration(
        nullptr), t1(), t2(), t3() {}

void godot::aed_point::_register_methods() {
    register_property<aed_point, float>("from_time", &aed_point::set_from_time, &aed_point::get_from_time, 0.0f);
    register_property<aed_point, float>("x", &aed_point::set_x, &aed_point::get_x, 0.0f);
    register_property<aed_point, float>("y", &aed_point::set_y, &aed_point::get_y, 0.0f);
    register_property<aed_point, float>("z", &aed_point::set_z, &aed_point::get_z, 0.0f);
    register_property<aed_point, float>("length", &aed_point::set_length, &aed_point::get_length, 0.0f);
    register_property<aed_point, float>("speed", &aed_point::set_speed, &aed_point::get_speed, 0.0f);
    register_property<aed_point, aed_acceleration*>("start_acceleration", &aed_point::set_start_acceleration, &aed_point::get_start_acceleration,
                                                    nullptr);
    register_property<aed_point, aed_acceleration*>("end_acceleration", &aed_point::set_end_acceleration, &aed_point::get_end_acceleration,
                                                    nullptr);
    register_property<aed_point, Vector3>("t1", &aed_point::set_t1, &aed_point::get_t1, Vector3());
    register_property<aed_point, Vector3>("t2", &aed_point::set_t2, &aed_point::get_t2, Vector3());
    register_property<aed_point, Vector3>("t3", &aed_point::set_t3, &aed_point::get_t3, Vector3());
}

void godot::aed_point::_init() noexcept {
    start_acceleration = aed_acceleration::_new();
    end_acceleration = aed_acceleration::_new();
}


// === Getters & setters
float godot::aed_point::get_from_time() const noexcept {
    return from_time;
}

void godot::aed_point::set_from_time(float from_time) noexcept {
    this->from_time = from_time;
}

float godot::aed_point::get_x() const noexcept {
    return x;
}

void godot::aed_point::set_x(float x) noexcept {
    this->x = x;
}

float godot::aed_point::get_y() const noexcept {
    return y;
}

void godot::aed_point::set_y(float y) noexcept {
    this->y = y;
}

float godot::aed_point::get_z() const noexcept {
    return z;
}

void godot::aed_point::set_z(float z) noexcept {
    this->z = z;
}

godot::aed_acceleration *godot::aed_point::get_start_acceleration() const {
    return start_acceleration;
}

void godot::aed_point::set_start_acceleration(godot::aed_acceleration *start_acceleration) {
    start_acceleration = start_acceleration;
}

godot::aed_acceleration *godot::aed_point::get_end_acceleration() const {
    return end_acceleration;
}

void godot::aed_point::set_end_acceleration(godot::aed_acceleration *end_acceleration) {
    end_acceleration = end_acceleration;
}

float godot::aed_point::get_speed() const {
    return speed;
}

void godot::aed_point::set_speed(float speed) {
    aed_point::speed = speed;
}

float godot::aed_point::get_length() const {
    return length;
}

void godot::aed_point::set_length(float length) {
    aed_point::length = length;
}

godot::Vector3 godot::aed_point::get_t1() const {
    return t1;
}

void godot::aed_point::set_t1(godot::Vector3 t1) {
    this->t1 = t1;
}

godot::Vector3 godot::aed_point::get_t2() const {
    return t2;
}

void godot::aed_point::set_t2(godot::Vector3 t2) {
    this->t2 = t2;
}

godot::Vector3 godot::aed_point::get_t3() const {
    return t3;
}

void godot::aed_point::set_t3(godot::Vector3 t3) {
    this->t3 = t3;
}

godot::aed_point::~aed_point() {
    start_acceleration->free();
    end_acceleration->free();
}
