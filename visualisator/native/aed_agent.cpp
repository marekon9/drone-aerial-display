#include "aed_agent.h"

void godot::aed_agent::_init() {
    //empty
}

void godot::aed_agent::_register_methods() {
    register_property<godot::aed_agent, Array>("path", &godot::aed_agent::set_path, &godot::aed_agent::get_path, Array());
    register_property<godot::aed_agent, Array>("light_path", &godot::aed_agent::set_light_path, &godot::aed_agent::get_light_path, Array());
}

godot::Array godot::aed_agent::get_path() const noexcept {
    return path;
}

void godot::aed_agent::set_path(godot::Array path) noexcept {
    this->path = path;
}

godot::Array &godot::aed_agent::get_path_ref() {
    return path;
}

godot::Array godot::aed_agent::get_light_path() const noexcept {
    return light_path;
}

godot::Array &godot::aed_agent::get_light_path_ref() noexcept {
    return light_path;
}

void godot::aed_agent::set_light_path(godot::Array light_path) noexcept {
    this->light_path = light_path;
}
