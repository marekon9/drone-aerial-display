#include "aed_bridge.h"

#include <vector>
#include "../../algorithm/aerial_display.h"

void godot::aed_bridge::_init() {
    // empty
}

void godot::aed_bridge::_register_methods() {
    register_method("compute_paths", &aed_bridge::compute_paths);
}

bool godot::aed_bridge::compute_paths(aed_parameters *params) {
    const aed::parameters * aed_params = params->getParams();
    if (params->isDirty() || aed_params == nullptr) {
        bool reloaded = params->reload_inner_parameters({});
        if (!reloaded) {
            return false;
        }
        aed_params = params->getParams();
    }

    aed::aerial_display display(aed_params->get_graph());
    auto paths = display.display_frames(aed_params->get_frames());

    return params->reload_inner_parameters(std::move(paths));
}

