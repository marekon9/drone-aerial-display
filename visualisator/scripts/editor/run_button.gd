extends Button

const drone_scene = preload("res://scenes/drone_scene.tscn")

signal display_ready

func _pressed():
	var edit_node = $"../../.."
	var agent_paths 
	if edit_node.params.has_path():
		agent_paths = edit_node.params.get_paths()
	else:
		var bridge = aed_bridge.new()
		var computed = bridge.compute_paths(edit_node.params)
		
		if not computed:
			return
		
		agent_paths = edit_node.params.get_paths()
	
	if len(agent_paths) == 0:
		return
	
	emit_signal("display_ready")

	var settings = edit_node.params.settings
	for agent in agent_paths:
		var drone = drone_scene.instance()
		drone.scale.x = settings.drone_length / 1000.0
		drone.scale.y = settings.drone_height / 1000.0
		drone.scale.z = settings.drone_width / 1000.0
		get_tree().root.get_node("Spatial").add_child(drone)
		drone.setup(settings.grid_size, settings.curvature, agent.path, agent.light_path)

