extends Panel
class_name grid_panel

enum view_mode {
	FIXED_HEIGHT, FIXED_WIDTH, FIXED_LENGTH
}

var row: int = 0
var col: int = 0
var rows: int = 0

var color_set: bool = false
var color

var static_coord: int = 0
var mode: int = 0

var grid_style = preload("res://styles/grid.tres").duplicate()
var focus_style = preload("res://styles/cell_focused.tres").duplicate()

signal cell_focused(cell)
signal cell_unfocused(cell)

func _init(row: int, col: int, rows: int, cols: int, static_coord: int, view_mode: int):
	self.row = row
	self.col = col
	self.rows = rows
	self.static_coord = static_coord
	self.mode = view_mode
	self.color_set = false
	self.color = Color(0,0,0,1)
	
	set_size(Vector2(32, 32))
	rect_min_size = Vector2(32, 32)
	add_stylebox_override("panel", grid_style)
	focus_mode = Control.FOCUS_ALL
	
	if row == rows - 1:
		grid_style.border_width_bottom = 1
	
	if col == cols - 1:
		grid_style.border_width_right = 1
	
	var label = Label.new()
	label.align = Label.ALIGN_CENTER
	label.valign = Label.VALIGN_CENTER

	add_child(label)
	
	hint_tooltip = compute_tooltip()
	
	connect("focus_entered", self, "_on_GridPanel_focus_entered")
	connect("focus_exited", self, "_on_GridPanel_focus_exited")

func compute_tooltip():
	var coord = get_coordinates()
	var text = "Coordinates: [" + str(coord[0]) + ", " + str(coord[1]) + ", " + str(coord[2]) + "]\n"
	
	if color_set:
		var color = get_rect_color()
		text += "Color RGB: [ "+ str(color.r8) + ", " + str(color.g8) +", " + str(color.b8) + "]\n"
		text += "Color visible: " + str(get_visibility())
	else:
		text += "Color is not set."
	
	var inner_text = get_text()
	if inner_text != '':
		text += "\nDrone: " + get_text()
	
	return text

func get_coordinates():
	var row_coord = rows - row - 1
	match mode:
		view_mode.FIXED_HEIGHT:
			return [col, row_coord, static_coord]
		view_mode.FIXED_WIDTH:
			return [col, static_coord, row_coord]
		view_mode.FIXED_LENGTH:
			return [static_coord, col, row_coord]
	return null

func _on_GridPanel_focus_entered():
	add_stylebox_override("panel", focus_style)
	emit_signal("cell_focused", self)

func _on_GridPanel_focus_exited():
	add_stylebox_override("panel", grid_style)
	emit_signal("cell_unfocused", self)

func get_rect_color():
	return color

func set_rect_color(color: Color):
	var label : Label = get_child(0)
	color_set = true
	self.color = color
	
	if color.a > 0:
		label.add_color_override("font_color", color.inverted())
	else:
		label.set("custom_colors/font_color", null)

	grid_style.bg_color = color
	focus_style.bg_color = color
	hint_tooltip = compute_tooltip()

func remove_rect_color():
	var label: Label = get_child(0)
	color_set = false
	label.text = ""
	label.set("custom_colors/font_color", null)
	
	color.a = 1
	grid_style.bg_color.a = 0
	focus_style.bg_color.a = 0
	hint_tooltip = compute_tooltip()

func has_rect_color():
	return color_set

func set_visibility(visible: bool):
	var label : Label = get_child(0)
	color.a = 1 if visible else 0
	if color_set:
		if visible:
			grid_style.bg_color.a = 1
			focus_style.bg_color.a = 1
			label.add_color_override("font_color", get_rect_color().inverted())
		else:
			grid_style.bg_color.a = 0
			focus_style.bg_color.a = 0
			label.set("custom_colors/font_color", null)
	hint_tooltip = compute_tooltip()

func get_visibility():
	return color.a > 0

func get_text():
	return get_child(0).text

func set_text(text: String):
	get_child(0).text = text
	hint_tooltip = compute_tooltip()
