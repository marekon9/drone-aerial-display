extends GridContainer

enum view_mode {
	FIXED_HEIGHT, FIXED_WIDTH, FIXED_LENGTH
}

func load_data(parent, rows: int, cols: int, static_coord: int, mode: int, positions: Array):
	clear()

	columns = cols + 1
	
	for i in range(rows):
		var label = Label.new()
		label.text = str(rows - i - 1)
		add_child(label)
		
		for j in range(cols):
			var box = grid_panel.new(i, j, rows, cols, static_coord, mode)
			box.connect("cell_focused", parent, "_on_cell_focused")
			box.connect("cell_unfocused", parent, "_on_cell_unfocused")
			add_child(box)

	add_child(Label.new())
	
	for i in range(cols):
		var label = Label.new()
		label.text = str(i)
		add_child(label)
	
	var prev: Panel = null
	var current: Panel = null
	var idx = 0

	for i in range(rows):
		idx += 1
		for j in range(cols):
			current = get_child(idx)
			if (prev != null):
				prev.focus_neighbour_right = current.get_path()
				current.focus_neighbour_left = prev.get_path()
			
			if (i > 0):
				var upper : Panel = get_child(idx - cols - 1)
				upper.focus_neighbour_bottom = current.get_path()
				current.focus_neighbour_top = upper.get_path()
			prev = current
			idx += 1
	
	var res_fce
	match mode:
		view_mode.FIXED_HEIGHT:
			res_fce = "fixed_height_resolution"
		view_mode.FIXED_WIDTH:
			res_fce = "fixed_width_resolution"
		view_mode.FIXED_LENGTH:
			res_fce = "fixed_length_resolution"
	
	add_pixels(rows, cols, static_coord, res_fce, positions)
	

func add_pixels(rows: int, cols: int, static_coordinate: int, fce: String, positions: Array):
	for pos in positions:
		var res = call(fce, pos.x, pos.y, pos.z, static_coordinate)
		if res != null:
			var row = rows - res[0] - 1
			var col = res[1]
			var idx = row * (cols + 1) + col + 1
			
			var box : grid_panel = get_child(idx)
			box.set_rect_color(pos.color)
			box.set_text("Any" if pos.agent < 0 else str(pos.agent))
			

func fixed_height_resolution(x, y, z, static_coordinate):
	if z == static_coordinate:
		return [y, x]
	return null

func fixed_width_resolution(x, y, z, static_coordinate):
	if y == static_coordinate:
		return [z, x]
	return null

func fixed_length_resolution(x, y, z, static_coordinate):
	if x == static_coordinate:
		return [z, y]
	return null

func clear():
	for child in get_children():
		remove_child(child)
		child.queue_free()
