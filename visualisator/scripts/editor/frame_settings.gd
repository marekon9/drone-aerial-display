extends VSplitContainer
class_name frames

var current_frame: int = 0
var static_coord: int = 0

var params: aed_parameters

enum frame_view_mode {
	FIXED_HEIGHT, FIXED_WIDTH, FIXED_LENGTH
}

var focused: grid_panel = null
var current_view_mode: int = 0

func _ready():
	pass

func edit_reload():
	# Check max coordinate
	var max_axis
	match current_view_mode:
		frame_view_mode.FIXED_HEIGHT:
			max_axis = params.settings.height - 1
		frame_view_mode.FIXED_WIDTH:
			max_axis = params.settings.width - 1
		frame_view_mode.FIXED_LENGTH:
			max_axis = params.settings.length - 1
	
	var coord = max_axis if static_coord > max_axis else static_coord
	
	# Clear unacessible positions
	for frame in params.frames:
		var positions: Array = frame.positions
		
		var idx = 0
		while idx < len(positions):
			var pos = positions[idx]
			if pos.x >= params.settings.length or pos.y >= params.settings.width or pos.z >= params.settings.height:
				positions.remove(idx)
				continue
			if pos.agent >= params.drone_count:
				pos.agent = -1
			idx += 1
	
	load_data(params, current_frame, coord, current_view_mode)

func load_data(params: aed_parameters, frame: int, coord: int, view_mode: int):
	current_frame = frame
	focused = null
	self.params = params
	static_coord = coord
	current_view_mode = view_mode
	
	setup_frame_settings()
	show_pixels()
	setup_coord_settings()
	show_coord()
	setup_drone_picker(params)

func setup_frame_settings():
	var max_frame = len(params.frames) - 1
	$FrameBar/CurrentFrame.value = current_frame
	$FrameBar/CurrentFrame.max_value = max_frame
	$FrameBar/MaxLabel.text = str(max_frame)
	
	$FrameBar/DeleteButton.disabled = len(params.frames) <= 3 or current_frame == 0 or current_frame == max_frame

func setup_coord_settings():
	var x_tooltip = "Axis x (length)"
	var y_tooltip = "Axis y (width)"
	var z_tooltip = "Axis z (height)"
	
	var col_label = "x"
	var col_tooltip = x_tooltip
	var row_label = "z"
	var row_tooltip = z_tooltip
	var static_label
	var static_tooltip
	var max_axis
	var rows
	var cols
	match current_view_mode:
		frame_view_mode.FIXED_HEIGHT:
			row_label = "y"
			row_tooltip = y_tooltip
			static_label = "z"
			static_tooltip = z_tooltip
			max_axis = params.settings.height - 1
			rows = params.settings.width
			cols = params.settings.length
		frame_view_mode.FIXED_WIDTH:
			static_label = "y"
			static_tooltip = y_tooltip
			max_axis = params.settings.width - 1
			rows = params.settings.height
			cols = params.settings.length
		frame_view_mode.FIXED_LENGTH:
			col_label = "y"
			col_tooltip = y_tooltip
			static_label = "x"
			static_tooltip = x_tooltip
			max_axis = params.settings.length - 1
			rows = params.settings.height
			cols = params.settings.width
			
	
	var static_axis = $FrameSplit/FrameContent/FrameViewSettings/ViewCoordinateContainer/AxisLabel
	static_axis.text = static_label
	static_axis.hint_tooltip = static_tooltip
	
	var col_axis = $FrameSplit/FrameContent/FrameDisplay/ColAxis
	col_axis.text = col_label
	col_axis.hint_tooltip = col_tooltip
	
	var row_axis = $FrameSplit/FrameContent/FrameDisplay/RowAxis
	row_axis.text = row_label
	row_axis.hint_tooltip = row_tooltip
	
	var edge = current_frame == 0 or current_frame >= len(params.frames) - 1
	if edge:
		max_axis = 0
	
	var coord_node = $FrameSplit/FrameContent/FrameViewSettings/ViewCoordinateContainer/CurrentCoordinates
	coord_node.value = static_coord
	coord_node.max_value = max_axis
	
	var picker = $FrameSplit/FrameContent/FrameViewSettings/ViewPicker
	picker.set_item_disabled(1, edge)
	picker.set_item_disabled(2, edge)
	picker.selected = current_view_mode
	
	$FrameSplit/FrameContent/FrameViewSettings/ViewCoordinateContainer/MaxCoordinate.text = str(max_axis)
	
	$FrameSplit/FrameContent/FrameDisplay/FrameScroller/FrameGrid.load_data(self, rows, cols, static_coord, current_view_mode, params.frames[current_frame].positions)

func show_pixels():
	var frame = params.frames[current_frame]
	$FrameSplit/FrameContent/FrameStats/CurrentPixelsLabel.text = str(len(frame.positions))
	$FrameSplit/FrameContent/FrameStats/MaxPixelsLabel.text = str(params.drone_count)

func setup_drone_picker(params: aed_parameters):
	var node = $FrameSplit/ColorSettings/ColorBar/DroneSelection
	node.clear()
	node.add_item("Any")
	
	for i in range(params.drone_count):
		node.add_item("Drone " + str(i))
		
	node.set_item_disabled(0, current_frame == 0)
	
	for pos in params.frames[current_frame].positions:
		if pos.agent >= 0:
			node.set_item_disabled(pos.agent + 1, true)
	
	pick_first_drone_available()

func pick_first_drone_available():
	var node = $FrameSplit/ColorSettings/ColorBar/DroneSelection
	
	for idx in range(node.get_item_count()):
		if not node.is_item_disabled(idx):
			node.selected = idx
			return
	node.selected = 0

func _on_cell_focused(panel: grid_panel):
	focused = panel
	show_coord()
	
	if panel.has_rect_color():
		if panel.get_visibility():
			var color_picker = $FrameSplit/ColorSettings/ColorPicker
			color_picker.color = panel.get_rect_color()
	
	var idx = find_position()
	var node = $FrameSplit/ColorSettings/ColorBar/DroneSelection
	if idx != null:
		var agent = params.frames[current_frame].positions[idx].agent
		node.selected = agent + 1
	else:
		pick_first_drone_available()
	$FrameSplit/ColorSettings/ColorBar/VisibleBox.pressed = focused.get_visibility()
	
func show_node_text():
	if focused != null:
		var idx = find_position()
		if idx != null:
			var agent = params.frames[current_frame].positions[idx].agent
			focused.set_text("Any" if agent < 0 else str(agent))
		else:
			focused.set_text("")
		

func show_coord():
	var text
	
	if focused != null:
		var coord = focused.get_coordinates()
		text = "[" + str(coord[0]) + ", " + str(coord[1]) + ", " + str(coord[2]) + "]"
	else:
		text = "[x, y, z]"
	$FrameSplit/ColorSettings/PositionGrid/PositionLabel.text = text

func _on_cell_unfocused(panel: grid_panel):
	pass

func _on_ColorPicker_color_changed(color: Color):
	if focused != null and (focused.get_visibility() or not focused.has_rect_color()) and edit_enabled():
		focused.set_rect_color(color)
	
		var pos = find_position()
		var coord = focused.get_coordinates()
		var positions : Array = params.frames[current_frame].positions
		
		if pos == null:
			var selection = $FrameSplit/ColorSettings/ColorBar/DroneSelection
			
			pos = aed_drone_position.new()
			pos.agent = selection.selected - 1
			pos.x = coord[0]
			pos.y = coord[1]
			pos.z = coord[2]
			positions.append(pos)
			
			drone_disabled(pos.agent, true)
			show_pixels()
			show_node_text()
		else:
			pos = positions[pos]
		pos.color = color
		params.dirty = true

func edit_enabled():
	return len(params.frames[current_frame].positions) < params.drone_count or (
		focused != null and focused.has_rect_color()
	)

func drone_disabled(num: int, disabled: bool):
	if num < 0:
		return
	
	var node = $FrameSplit/ColorSettings/ColorBar/DroneSelection
	node.set_item_disabled(num + 1, disabled)

func _on_ClearButton_pressed():
	if focused != null:
		focused.remove_rect_color()
		
		var idx = find_position()
		if idx != null:
			var positions : Array = params.frames[current_frame].positions
			var pos = positions[idx]
			drone_disabled(pos.agent, false)
			
			positions.remove(idx)
			
			params.dirty = true
			
			show_pixels()
			_on_cell_focused(focused)

func find_position():
	if focused == null:
		return null
	
	var coord = focused.get_coordinates()
	var idx = 0
	for pos in params.frames[current_frame].positions:
		if pos.x == coord[0] and pos.y == coord[1] and pos.z == coord[2]:
			return idx
		idx += 1
	return null


func _on_VisibleBox_toggled(button_pressed: bool):
	if focused != null:
		focused.set_visibility(button_pressed)
		
		var pos = find_position()
		if pos != null:
			var positions : Array = params.frames[current_frame].positions
			pos = positions[pos]
			pos.color.a = 1 if button_pressed else 0
			params.dirty = true
		


func _on_DroneSelection_item_selected(index: int):
	if focused != null:
		var idx = find_position()
		if idx != null:
			var pos = params.frames[current_frame].positions[idx]
			var agent = index - 1
			
			drone_disabled(pos.agent, false)
			drone_disabled(agent, true)
			
			pos.agent = agent
			
			show_node_text()
			params.dirty = true

func _on_CurrentFrame_value_changed(value):
	var idx = int(value)
	var max_frame = len(params.frames) - 1
	var mode = current_view_mode
	var coord = static_coord
	if idx == 0 or idx >= max_frame:
		mode = frame_view_mode.FIXED_HEIGHT
		coord = 0
	
	load_data(params, idx, coord, mode)


func _on_CurrentCoordinates_value_changed(value):
	var idx = int(value)
	load_data(params, current_frame, idx, current_view_mode)


func _on_ClearFrame_pressed():
	var positions: Array = params.frames[current_frame].positions
	positions.clear()
	params.dirty = true
	load_data(params, current_frame, static_coord, current_view_mode)


func _on_Duplicate_pressed():
	var frame: aed_frame = aed_frame.new()
	frame.positions = []
	
	params.dirty = true
	for pos in params.frames[current_frame].positions:
		var new_pos = aed_drone_position.new()
		new_pos.agent = pos.agent
		new_pos.color = Color(pos.color.r, pos.color.g, pos.color.b, pos.color.a)
		new_pos.x = pos.x
		new_pos.y = pos.y
		new_pos.z = pos.z
		
		frame.positions.append(new_pos)
	
	params.frames.insert(current_frame + 1, frame)
	setup_frame_settings()


func _on_DeleteButton_pressed():
	params.frames.remove(current_frame)
	params.dirty = true
	
	var mode = current_view_mode
	var coord = static_coord
	
	if current_frame >= len(params.frames) - 1:
		mode = frame_view_mode.FIXED_HEIGHT
		coord = 0
	
	load_data(params, current_frame, coord, mode)


func _on_InsertButton_pressed():
	var frame = aed_frame.new()
	frame.positions = []
	params.frames.insert(current_frame, frame)
	params.dirty = true
	
	current_frame += 1
	setup_frame_settings()


func _on_AppendButton_pressed():
	var frame = aed_frame.new()
	frame.positions = []
	params.frames.insert(current_frame + 1, frame)
	params.dirty = true
	
	setup_frame_settings()


func _on_ViewPicker_item_selected(index):
	var coord = 0
	if focused != null:
		var focus_coord = focused.get_coordinates()
		match index:
			frame_view_mode.FIXED_HEIGHT:
				coord = focus_coord[2]
			frame_view_mode.FIXED_WIDTH:
				coord = focus_coord[1]
			frame_view_mode.FIXED_LENGTH:
				coord = focus_coord[0]
	
	load_data(params, current_frame, coord, index)
