extends Tree

var length : TreeItem
var width : TreeItem
var height : TreeItem
var k : TreeItem
var precision : TreeItem
var curvature : TreeItem
var grid_size : TreeItem
var drone_count : TreeItem
var max_speed : TreeItem
var max_acceleration: TreeItem
var drone_length : TreeItem
var drone_width : TreeItem
var drone_height : TreeItem
var stop_penalty : TreeItem
var distance_multiplier : TreeItem
var wait_time : TreeItem

var batch_size: TreeItem
var batch_delay: TreeItem
var batch_distance: TreeItem

func _ready():
	var root : TreeItem = create_item()
	set_hide_root(true)
	
	var graph = create_item(root)
	graph.set_text(0, "Graph")
	
	length = create_int_child(graph, "Length", "Number of pixels in length dimension.")
	width = create_int_child(graph, "Width", "Number of pixels in width dimension.")
	height = create_int_child(graph, "Height", "Number of pixels in height dimension.")
	k = create_int_child(graph, "K", "Min value two. Defines density of the display graph. Higher values means denser graph.", 3, 2)
	precision = create_int_child(graph, "Precision", "Precision for computing trajectory length. Higher number means more precise trajectory computation.", 100, 1)
	curvature = create_int_child(graph, "Curvature", "Defines curvature of the trajectory. Higher value tends to make longer curves.", 1, 0.1, 0.1)
	grid_size = create_int_child(graph, "Grid size", "Defines size of the graph unit edge (in millimetres).", 1000)
	
	var drones = create_item(root)
	drones.set_text(0, "Drones")
	drone_count = create_int_child(drones, "Count", "How many drones are available.")
	max_speed = create_int_child(drones, "Maximum speed", "The maximum speed of drones measured in mm/ms.", 1, 0.1, 0.1)
	max_acceleration = create_int_child(drones, "Maximum acceleration", "The maximum acceleration of the drones measured in mm * ms^(-2).", 1, 0.1, 0.1)
	drone_length = create_int_child(drones, "Length radius", "Length radius of the drone (in millimitres). Maximum: half of the grid size.", 300)
	drone_width = create_int_child(drones, "Width radius", "Width radius of the drone (in millimitres). Maximum: half of the grid size.", 300)
	drone_height = create_int_child(drones, "Height radius", "Height radius of the drone (in millimitres). Maximum: half of the grid size.", 500)
	stop_penalty = create_int_child(drones, "Stop penalty", "Time penalty (in milliseconds) for stopping the drone while moving.", 0, 0)
	
	var frames = create_item(root)
	frames.set_text(0, "Frames")
	wait_time = create_int_child(frames, "Wait time", "How long will drones stay in given position (in milliseconds).", 5, 0, 0.1)
	
	var hyperparams = create_item(root)
	hyperparams.set_text(0, "Hyper parameters")
	distance_multiplier = create_int_child(hyperparams, "Distance multiplier", "Sets how impartant is measure of remaining distance to the path finding algorithm. Examples: 0 = the algorithm will not concider remaining path. 1 = the remaining path is concidered approximately equal to the already computed trajectory time. Recommended values: lesser multiples of 1 (e.g. 5 or 10) for finding paths quickly. Value of 1 is usually slow.",10.0, 0, 0.1 )
	batch_size = create_int_child(hyperparams, "Batch size", "Sets the maximum number of drones which take off or land simultaneosly. This feature may help to reduce computation time drastically. Set to 0 to turn the feature off.", 30, 0, 1)
	batch_delay = create_int_child(hyperparams, "Batch delay", "Delay (in milliseconds) between launching / landing batches of drones.", 3000)
	batch_distance = create_int_child(hyperparams, "Batch distance", "Within batch there should not drones whose starting or final position is shorter then given number (the number is equal to distance / grid_size).", 3.0, 1.0, 0.1)


func create_int_child(parent : TreeItem, name: String, tooltip: String, default : int = 10, minimum : int = 1, step : int = 1):
	var child = create_item(parent)
	child.set_text(0, name)
	child.set_tooltip(0, tooltip)
	child.set_cell_mode(1, TreeItem.CELL_MODE_RANGE)
	child.set_range_config(1, minimum, 1000000000, step)
	child.set_range(1, default)
	child.set_editable(1, true)
	return child

func load_data(params: aed_parameters):
	length.set_range(1, params.settings.length)
	width.set_range(1, params.settings.width)
	height.set_range(1, params.settings.height)
	k.set_range(1, params.settings.k)
	precision.set_range(1, params.settings.precision)
	curvature.set_range(1, params.settings.curvature)
	grid_size.set_range(1, params.settings.grid_size)
	
	max_speed.set_range(1, params.settings.max_speed)
	max_acceleration.set_range(1, params.settings.max_acceleration)
	
	drone_length.set_range(1, params.settings.drone_length)
	drone_width.set_range(1, params.settings.drone_width)
	drone_height.set_range(1, params.settings.drone_height)
	drone_count.set_range(1, params.drone_count)
	
	stop_penalty.set_range(1, params.settings.stop_penalty)
	
	wait_time.set_range(1, params.wait_time)
	
	distance_multiplier.set_range(1, params.settings.distance_multiplier)
	batch_size.set_range(1, params.settings.batches)
	batch_delay.set_range(1, params.settings.batch_delay)
	batch_distance.set_range(1, params.settings.batch_distance)


func _on_Display_Settings_item_edited():
	var params = $"../..".params
	params.dirty = true
	
	params.settings.length = length.get_range(1)
	params.settings.width = width.get_range(1)
	params.settings.height = height.get_range(1)
	params.settings.k = k.get_range(1)
	params.settings.precision = precision.get_range(1)
	params.settings.curvature = curvature.get_range(1)
	params.settings.grid_size = grid_size.get_range(1)
	
	params.settings.max_speed = max_speed.get_range(1)
	params.settings.max_acceleration = max_acceleration.get_range(1)
	
	params.settings.drone_length = drone_length.get_range(1)
	params.settings.drone_width = drone_width.get_range(1)
	params.settings.drone_height = drone_height.get_range(1)
	
	params.settings.stop_penalty = stop_penalty.get_range(1)
	params.settings.distance_multiplier = distance_multiplier.get_range(1)
	
	params.drone_count = drone_count.get_range(1)
	params.wait_time = wait_time.get_range(1)
	
	params.settings.batches = batch_size.get_range(1)
	params.settings.batch_delay = batch_delay.get_range(1)
	params.settings.batch_distance = batch_distance.get_range(1)

	$"../Frames".edit_reload()
	
	
