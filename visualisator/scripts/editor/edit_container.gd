extends VSplitContainer

var params : aed_parameters = aed_parameters.new()
var display_ready = false

func _init():
	connect_params()

func connect_params():
	params.connect("parse_error", self, "_on_params_parsing_error")
	params.connect("general_error", self, "_on_params_general_error")


func new_display():
	params = aed_parameters.new()
	connect_params()
	params.frames = [aed_frame.new(), aed_frame.new(), aed_frame.new()]
	load_data()

func load_display(file: String):
	var loaded = params.load_from_file(file)
	if not loaded:
		return false
	
	load_data()
	return true

func load_data():
	$"DataContainer/Display Settings".load_data(params)
	$"DataContainer/Frames".load_data(params, 0, 0, 0)
	
	
func _on_Display_ready():
	visible = false
	display_ready = true
	
func _input(event):
	if display_ready and Input.is_key_pressed(KEY_ESCAPE):
		display_ready = false
		visible = true


func _on_Save_pressed():
	$SaveDialog.popup_centered()

func _on_SaveDialog_file_selected(path: String):	
	var include_paths = $BottomBar/BottomRightBar/TrajectoryCheckbox.is_pressed()
	
	if include_paths and not params.has_path():
		var computed = aed_bridge.new().compute_paths(params)
		if not computed:
			return
	
	params.store_to_file(path, include_paths)

func _on_params_parsing_error(lineNum: int, line: String, reason: String):
	var text = "Could not parse the file.\nProblem:\nLine: " + str(lineNum) + "\nContent: " \
	+ line + "\nReason: " + reason
	
	$ErrorDialog.dialog_text = text
	$ErrorDialog.popup_centered()

func _on_params_general_error(reason: String):
	$ErrorDialog.dialog_text = reason
	$ErrorDialog.popup_centered()
