extends KinematicBody

var speed: float = 20
var sensitivity: float = 0.01

var moving: bool = false

func _ready():
	pass

func _physics_process(delta):
#	if not moving:
#		return
	var vector: Vector3 = Vector3()
	if Input.is_action_pressed("ui_up") :
		vector -= transform.basis.z
	elif Input.is_action_pressed("ui_down"):
		vector += transform.basis.z
	elif Input.is_action_pressed("ui_left"):
		vector -= transform.basis.x
	elif Input.is_action_pressed("ui_right"):
		vector += transform.basis.x
		
	move_and_slide(vector * speed, Vector3.UP)
	
func _input(event):
	if not moving:
		return
	if event is InputEventMouseMotion:
		self.rotate_y(-event.relative.x * sensitivity)
	elif Input.is_key_pressed(KEY_ESCAPE):
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
		moving = false

func _on_Display_Ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	moving = true
