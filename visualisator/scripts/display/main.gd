extends Spatial


func _ready():
	var run_node = $"Editor/EditContainer/BottomBar/BottomRightBar/Run"
	run_node.connect("display_ready", $CameraPlatform, "_on_Display_Ready")
	run_node.connect("display_ready", $Editor/EditContainer, "_on_Display_ready")
