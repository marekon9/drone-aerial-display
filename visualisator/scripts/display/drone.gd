extends KinematicBody

var curvature
var grid_size

var movements = []
var i = 0

var lights = []
var l = 0
var start_time

func _ready():
	start_time = OS.get_ticks_msec()	
	

func setup(grid_size, curvature, movements: Array, lights: Array):
	self.grid_size = grid_size
	self.curvature = curvature
	self.movements = movements
	self.lights = lights
	
func process_iterator(j: int, time: int, arr: Array):
	while j < len(arr) - 1:
		var next = arr[j + 1]
		
		if next.from_time <= time:
			j += 1
		else:
			break
	
	return j

func pos_same(pos_a, pos_b):
	return pos_a.x == pos_b.x and pos_a.y == pos_b.y and pos_a.z == pos_b.z
	
func movement_length(relative_time, total_time, init_speed, start_acc, end_acc):
	if relative_time <= start_acc.duration:
		var res = (relative_time * relative_time * start_acc.acceleration + 2 * init_speed * relative_time) / 2
		return res

	var acc_length = (start_acc.duration * start_acc.duration * start_acc.acceleration + 2 * init_speed * start_acc.duration) / 2
	var rest_speed = start_acc.acceleration * start_acc.duration + init_speed
	
	if relative_time < total_time - end_acc.duration:
		var rest_time = relative_time - start_acc.duration
		var rest_length = rest_time * rest_speed
		var res = acc_length + rest_length
		return res
	
	var rest_time = total_time - start_acc.duration - end_acc.duration
	var rest_length = rest_speed * rest_time
	
	var end_time = relative_time - rest_time - start_acc.duration
	var end_acc_length = (end_time * end_time * end_acc.acceleration + 2 * rest_speed * end_time) / 2
	var res = acc_length + rest_length + end_acc_length

	return res

func line_movement(length, from, to):
	var mov_time = length / to.length
	var x = from.x + mov_time * (to.x - from.x)
	var y = from.y + mov_time * (to.y - from.y)
	var z = from.z + mov_time * (to.z - from.z)

	self.global_transform.origin.x = x
	self.global_transform.origin.y = y
	self.global_transform.origin.z = z

func curved_coordinate(coef, from, t1, t2, t3):
	return from + (t1 * coef + t2 * (coef * coef) + t3 * (coef * coef * coef)) / grid_size

func curved_movement(length, from, to):
	var coef = (length / to.length) * curvature

	var x = curved_coordinate(coef, from.x, to.t1.x, to.t2.x, to.t3.x)
	var y = curved_coordinate(coef, from.y, to.t1.y, to.t2.y, to.t3.y)
	var z = curved_coordinate(coef, from.z, to.t1.z, to.t2.z, to.t3.z)

	self.global_transform.origin.x = x
	self.global_transform.origin.y = y
	self.global_transform.origin.z = z

func process_movement(time: int):
	i = process_iterator(i, time, movements)
	
	var mov = movements[i]
	if i >= len(movements) - 1 or pos_same(mov, movements[i + 1]):
		global_transform.origin.x = mov.x
		global_transform.origin.y = mov.y
		global_transform.origin.z = mov.z
		return

	var next = movements[i + 1]
	
	var curr_position = global_transform.origin
	var length = movement_length(time - mov.from_time, next.from_time - mov.from_time, mov.speed, next.start_acceleration, next.end_acceleration)

	if next.t1.is_equal_approx(Vector3()) and next.t2.is_equal_approx(Vector3()) and next.t3.is_equal_approx(Vector3()):
		line_movement(length, mov, next)
	else:
		curved_movement(length, mov, next)


func process_light(time: int):
	l = process_iterator(l, time, lights)
	
	var light = lights[l]
	if l >= len(lights) - 1:
		var material : SpatialMaterial = $Bubble/Bulb.mesh.surface_get_material(0)
		material.albedo_color = light.color
		$Bubble/Bulb/Light.light_color = light.color
		$Bubble/Bulb.visible = light.color.a == 1
		return
	
	var next = lights[l + 1]
	var light_time = (time - light.from_time) / (next.from_time - light.from_time)
	
	var color = Color()
	color.r = light.color.r + light_time * (next.color.r - light.color.r)
	color.g = light.color.g + light_time * (next.color.g - light.color.g)
	color.b = light.color.b + light_time * (next.color.b - light.color.b)
	
	$Bubble/Bulb.mesh.surface_get_material(0).albedo_color = color
	$Bubble/Bulb/Light.light_color = color
	$Bubble/Bulb.visible = next.color.a == 1
	

func _physics_process(delta):
	if i >= len(movements):
		return
	
	var time = OS.get_ticks_msec() - start_time

	process_movement(time)
	process_light(time)

func _input(event):
	if Input.is_key_pressed(KEY_ESCAPE):
		queue_free()

