extends FileDialog

func _init():
	anchor_top = -0.2
	anchor_bottom = 0.2

func _on_FileDialog_file_selected(path: String):
	var editor_node = get_tree().root.get_node("Spatial/Editor/EditContainer")
	if editor_node.load_display(path):
		get_tree().root.get_node("Spatial/Menu").visible = false
		get_tree().root.get_node("Spatial/Editor").visible = true
