extends Button

func _pressed():
	get_tree().root.get_node("Spatial/Editor/EditContainer").new_display()
	
	get_tree().root.get_node("Spatial/Menu").visible = false
	get_tree().root.get_node("Spatial/Editor").visible = true
